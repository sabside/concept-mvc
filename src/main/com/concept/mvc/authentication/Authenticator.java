package com.concept.mvc.authentication;

import com.concept.mvc.exception.ServiceException;

public interface Authenticator {

	/**
	 * 
	 * @param username
	 * @param password
	 * @return
	 * @throws ServiceException
	 */
	public boolean authenticate(String username, String password) throws ServiceException;
}
