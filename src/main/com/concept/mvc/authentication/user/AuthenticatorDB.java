/**
 * 
 */
package com.concept.mvc.authentication.user;

/**
 * @author Sabside
 * 
 */
public interface AuthenticatorDB {

	public BasicAuthUser authenticate(String username, String password) throws Exception;
}
