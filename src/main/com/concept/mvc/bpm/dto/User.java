package com.concept.mvc.bpm.dto;

import java.io.Serializable;

public class User implements Serializable {

	private static final long serialVersionUID = -8985151751508252504L;
	private int id;
	private int superiorId;
	private User superior;

	/**
	 * Used to address them & others in the escalation process
	 */
	private String name;
	private String surname;

	/**
	 * Escalation Medium
	 */
	private String email;
	private String cellphone;

	public User(int id, String name, String email, String cellphone, User superior) {
		super();
		this.id = id;
		this.name = name;
		this.email = email;
		this.cellphone = cellphone;
		this.superior = superior;
	}

	public User() {
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public int getSuperiorId() {
		return superiorId;
	}

	public void setSuperiorId(int superiorId) {
		this.superiorId = superiorId;
	}

	public String getName() {
		return name;
	}

	public String getEmail() {
		return email;
	}

	public String getCellphone() {
		return cellphone;
	}

	public User getSuperior() {
		return superior;
	}

	public void setSuperior(User superior) {
		this.superior = superior;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setCellphone(String cellphone) {
		this.cellphone = cellphone;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
