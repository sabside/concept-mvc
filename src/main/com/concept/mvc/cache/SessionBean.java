package com.concept.mvc.cache;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Sometimes you want CDI to be sessionscoped but where the implementation there
 * is not servlet context and it fails/refuses to give the bean instance.
 * 
 * This implements a way to store the instance. To use it, let the container instantiate 
 * the instance by injecting it in your class then store it on this manager. It will be
 * stored in the session for you, and when the session expires, all the instances are gone. 
 * 
 * @author Sabside
 *
 */
public class SessionBean implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Map<String, Object> map = new HashMap<>();
	private long lastUpdate;
	
	public void put(String key, Object value) {
		map.put(key, value);
		lastUpdate = System.currentTimeMillis();
	}
	
	public Optional<Object> get(String key){
		return Optional.ofNullable(map.get(key));
	}
	
	public void renew() {
		lastUpdate = System.currentTimeMillis();
	}
	
	public long lastUpdate() {
		return lastUpdate;
	}
}
