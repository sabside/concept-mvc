package com.concept.mvc.cache;

import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 
 * @author Sabside
 *
 */
public class SessionBeanCleaner extends Thread {
	private static final Log log = LogFactory.getLog(SessionBeanCleaner.class);
	private static final long SESSION_TIMEOUT = 1800000;
	private static final long CLEANUP_INTERVAL = 300000;
	
	@Override
	public void run() {
		
		while (true) {
			log.debug("cleaning session beans...");
			Set<String> sessions = SessionBeanManager.getInstance().keySet();
			long time = System.currentTimeMillis();
			
			sessions.removeIf(sessionId->{
					return (time - SessionBeanManager.getInstance().get(sessionId).get().lastUpdate()) > SESSION_TIMEOUT;
				});
			
			try {
				Thread.sleep(CLEANUP_INTERVAL);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
