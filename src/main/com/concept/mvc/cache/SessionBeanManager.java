package com.concept.mvc.cache;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * 
 * @author Sabside
 *
 */
public class SessionBeanManager {

	private static SessionBeanManager instance;
	private Map<String, SessionBean> sessions = new HashMap<>();
	
	private SessionBeanManager() {}
	
	public static SessionBeanManager getInstance() {
		if (instance == null) {
			instance = new SessionBeanManager();
		}
		return instance;
	}
	
	public void put(String sessionId, SessionBean value) {
		sessions.put(sessionId, value);
	}
	
	public Optional<SessionBean> get(String sessionId){
		return Optional.ofNullable(sessions.get(sessionId));
	}
	
	public Set<String> keySet(){
		return sessions.keySet();
	}
	
	public void remove(String sessionId) {
		sessions.remove(sessionId);
	}
}
