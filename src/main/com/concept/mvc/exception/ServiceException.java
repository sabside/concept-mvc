package com.concept.mvc.exception;

public class ServiceException extends Exception {

	private boolean logout;
	
	private static final long serialVersionUID = 1L;

	public ServiceException() {
		
	}

	public ServiceException(String message, boolean logout) {
		super(message);
		this.logout = logout;				
	}
	
	public ServiceException(String message) {
		super(message);
		
	}

	public ServiceException(Throwable cause) {
		super(cause);
		
	}

	public ServiceException(String message, Throwable cause) {
		super(message, cause);
		
	}

	public ServiceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		
	}

	public boolean isLogout() {
		return logout;
	}
}
