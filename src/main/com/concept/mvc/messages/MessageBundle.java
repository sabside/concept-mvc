package com.concept.mvc.messages;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

import com.concept.mvc.navigation.controller.Controller;

/**
 * This expects a property file to be present in /WEB-INF/datafiles/messages.properties and must hold the error messages
 * 
 * @author F3557790
 *
 */
public class MessageBundle {

	private static MessageBundle instance;
	private Properties messages = new Properties();

	private MessageBundle() {
		// we load the message from properties file
		loadMessages();
	}

	private void loadMessages() {
		try {
			messages.load(new FileInputStream(new File(Controller.serverPath + "/WEB-INF/datafiles/messages.properties")));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static MessageBundle getInstance() {
		if (instance == null) {
			instance = new MessageBundle();
		}
		return instance;
	}

	public String getMessage(String messageId) {
		if (messages.getProperty(messageId) == null) {
			// we try and reload the messages if we can't find it
			loadMessages();
		}
		return messages.getProperty(messageId);
	}
}
