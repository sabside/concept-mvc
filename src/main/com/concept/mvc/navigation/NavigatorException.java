package com.concept.mvc.navigation;

@SuppressWarnings("serial")
public class NavigatorException extends Exception {

	public NavigatorException() {
	}

	public NavigatorException(String message) {
		super(message);
	}

	public NavigatorException(Throwable cause) {
		super(cause);
	}

	public NavigatorException(String message, Throwable cause) {
		super(message, cause);
	}

	public NavigatorException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
