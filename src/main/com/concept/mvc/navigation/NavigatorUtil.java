package com.concept.mvc.navigation;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import com.concept.mvc.service.User;
import com.concept.mvc.validation.ErrorMessage;
import com.concept.mvc.viewbean.ViewBean;
import com.concept.utils.exception.ValidationException;

/**
 * 
 * @author Sabside
 *
 */
public interface NavigatorUtil {

	void putToSession(Class t, Object object);
	
	Object getFromSession(Class t);
	
	boolean isPostLogin();
	
	void setPostLogin(boolean value);

	User getUser() throws Exception;
	/**
	
	 * sets the user as an attribute on the session with key "concept.user" 
	 * @param user
	 */
	void setUser(User user);
	
	boolean userExists();
	
	boolean setValidationErrorMessage(ErrorMessage error);
	
	void setValidationErrorMessage(String messageStr, String fieldDisplayName, String fieldId);
	
	void initialize(HttpServletRequest request, HttpServletResponse response);
	
	void handleService(Exception e);
	
	void handleError(Exception e);

	String getParam(String key);
	
	String getResponseType();
	
	String getJSONResponse();

	String buildSuccessMsg(String key, String value) throws Exception;

	String buildSuccessMsg(String key, int value) throws Exception;
	
	String buildSuccessMsg(String key, boolean value) throws Exception;
	
	void success() throws Exception;
	
	void success(String msg) throws Exception;
	
	void successForObject(Object object) throws Exception;
	
	void success(Collection object) throws Exception;
	
	void success(JSONArray array) throws Exception;
	
	void success(JSONObject object) throws Exception;

	void error(Collection object);
	
	void setResponseType(String responseType) ;

	void setJsonResponse(String jsonResponse);
	
	void setNextNavigator(String nav);
	
	String getNextNavigator();
	
	boolean validate(ViewBean  viewbean) throws ValidationException;
	
	void setToRequest(String key, Object value);

	void error(String msg);
	
	void bootOutUser() throws Exception;
}
