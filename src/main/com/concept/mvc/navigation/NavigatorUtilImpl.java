package com.concept.mvc.navigation;

import java.util.Collection;

import javax.annotation.Priority;
import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.concept.mvc.exception.ServiceException;
import com.concept.mvc.navigation.controller.Controller;
import com.concept.mvc.reporting.Reporter;
import com.concept.mvc.reporting.Severity;
import com.concept.mvc.service.User;
import com.concept.mvc.util.SessionValues;
import com.concept.mvc.validation.ErrorMessage;
import com.concept.mvc.validation.ErrorMessages;
import com.concept.mvc.viewbean.ViewBean;
import com.concept.utils.exception.ValidationException;

/**
 * 
 * @author Sabelo Simelane [Sabelo Simelane <sabside@gmail.com>]
 *
 **/
@RequestScoped
@Priority(1000)
@SuppressWarnings("rawtypes")
public class NavigatorUtilImpl implements NavigatorUtil {

	private static final Log log = LogFactory.getLog(Controller.class);

	public static final String JSP_RESPONSE = "JSP";
	public static final String JSON_RESPONSE = "JSON";
	public static final String PDF_RESPONSE = "PDF";
	
	private HttpServletRequest request;
	private HttpServletResponse response;
	private String responseType = JSP_RESPONSE;
	private String jsonResponse;
	private String nextNav;
	private boolean postLogin = true;
	
	@Override
	public boolean isPostLogin() {
		return postLogin;
	}
	
	@Override
	public void setPostLogin(boolean value) {
		postLogin = value;
	}

	@Override
	public User getUser() throws Exception {
		User user = (User) request.getSession().getAttribute("concept.user");
		
		if (user == null)  throw new ServiceException("Unauthorized (401)", true);
		
		return user;
	}
	
	/**
	 * sets the user as an attribute on the session with key "concept.user" 
	 * @param user
	 */
	@Override
	public void setUser(User user){
		this.request.getSession().setAttribute("concept.user", user);
	}

	@Override
	public boolean setValidationErrorMessage(ErrorMessage error) {
		if (!error.getMessage().isEmpty()) {
			setValidationErrorMessage(error.getMessage(), error.getFieldDisplayName(), error.getFieldId());
			return false;
		}
		return true;
	}

	@Override
	public void setValidationErrorMessage(String messageStr, String fieldDisplayName, String fieldId) {
		ErrorMessages messages = (ErrorMessages) this.request.getSession().getAttribute(SessionValues.ERROR_MESSAGES);
		if (messages == null) {
			messages = new ErrorMessages();
			this.request.getSession().setAttribute(SessionValues.ERROR_MESSAGES, messages);
		}
		ErrorMessage message = new ErrorMessage();
		message.setFieldDisplayName(fieldDisplayName);
		message.setFieldId(fieldId);

		message.setMessage(messageStr);

		messages.addMessage(fieldId, message);
	}

	@Override
	public void initialize(HttpServletRequest request, HttpServletResponse response) {
		log.info("Initializing NavigatorUtilImpl...");
		this.request = request;
		this.response = response;
	}

	@Override
	public void handleService(Exception e) {
		e.printStackTrace();
		log.info("handling service exception...");

		JSONObject json = null;
		JSONObject error = new JSONObject();
		try {
			json = new JSONObject();
			
			error.accumulate("message", e.getMessage() == null ? "An error occured, please try again." : e.getMessage());
			json.accumulate("message", e.getMessage() == null ? "An error occured, please try again." : e.getMessage());
			
			
			json.accumulate("response", HttpServletResponse.SC_BAD_REQUEST);
			json.accumulate("data", error);
			
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		
		responseType = JSON_RESPONSE;
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		jsonResponse = json.toString();
		
		ServiceException exception = (ServiceException)e;
		if (exception.isLogout()) {
			try {
				bootOutUser();
			} catch (Exception e1) {e1.printStackTrace();	}
		}
		
	}
	
	@Override
	public void handleError(Exception e) {
		if (e instanceof ServiceException) {
			handleService(e); return;
		} else if (e instanceof ValidationException) {
			log.info("handling a validation exception...");
			ValidationException ex = (ValidationException)e;
			if (ex.getFieldId() != null && ex.getFieldDisplayName() != null && ex.getMessageStr() != null) {
				setValidationErrorMessage(ex.getMessageStr(), ex.getFieldDisplayName(), ex.getFieldId());
			} else {
				handleService(ex);
			}
			return;
		}
		
		e.printStackTrace();
		log.info("handling error...");

		JSONObject json = null;
		JSONObject error = new JSONObject();
		try {
			error.put("message", "An error occured, please try again or contact the administrator.");
			json = new JSONObject();
			json.put("response", HttpServletResponse.SC_BAD_REQUEST);
			json.put("data", error);
			json.put("message", "An error occured, please try again or contact the administrator.");
		} catch (JSONException e1) {
			e1.printStackTrace();
		}

		responseType = JSON_RESPONSE;
		response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		jsonResponse = json.toString();

		new Reporter().report(Severity.WARNING, e, "Exception caught by Navigator");
	}

	@Override
	public String getParam(String key) {
		return request.getParameter(key);
	}

	@Override
	public String getResponseType() {
		return responseType;
	}

	@Override
	public String getJSONResponse() {
		return jsonResponse;
	}

	@Override
	public String buildSuccessMsg(String key, String value) throws Exception {
		JSONObject json = new JSONObject();
		
		JSONObject data = new JSONObject();
		json.accumulate("response", "success");
		json.accumulate("data", data);
		data.accumulate(key, value);

		return json.toString();
	}

	@Override
	public String buildSuccessMsg(String key, int value) throws Exception{
		JSONObject json = new JSONObject();
		
		JSONObject data = new JSONObject();
		json.accumulate("response", "success");
		json.accumulate("data", data);
		data.accumulate(key, value);

		return json.toString();
	}
	
	@Override
	public String buildSuccessMsg(String key, boolean value) throws Exception{
		JSONObject json = new JSONObject();
		
		JSONObject data = new JSONObject();
		json.accumulate("response", "success");
		json.accumulate("data", data);
		data.accumulate(key, value);

		return json.toString();
	}
	
	
	private String buildSuccessMsg() throws Exception {
		JSONObject json = new JSONObject();
		json.accumulate("response", "success");
		json.accumulate("data", "Server returned success.");

		return json.toString();
	}

	@Override
	public void success() throws Exception {
		this.setJsonResponse(buildSuccessMsg());
	}
	
	@Override
	public void success(String msg) throws Exception {
		this.setJsonResponse(buildSuccessMsg(msg));
	}
	
	private String buildSuccessMsg(String msg) throws Exception {
		JSONObject json = new JSONObject();
		json.accumulate("response", "success");
		if (msg.startsWith("{")){
			json.accumulate("data", new JSONObject(msg));
		} else {
			json.accumulate("data", msg);
		}

		return json.toString();
	}

	@Override
	public void successForObject(Object object) throws Exception {
		this.setJsonResponse(buildSuccessMsgForObject(object));
	}
	
	private String buildSuccessMsgForObject(Object object) throws Exception {
		JSONObject json = new JSONObject();
		json.accumulate("data", new JSONObject(object));
		json.accumulate("response", "success");

		return json.toString();
	}

	@Override
	public void success(Collection object) throws Exception {
		this.setJsonResponse(buildSuccessMsg(object));
	}
	
	private String buildSuccessMsg(Collection object) throws Exception {

		JSONObject json = new JSONObject();
		json.put("data", object);
		json.append("response", "success");

		return json.toString();
	}
	
	@Override
	public void success(JSONArray array) throws Exception{
		this.setJsonResponse(buildSuccessMsg(array));
	}
	
	private String buildSuccessMsg(JSONArray array) throws Exception{
		JSONObject json = new JSONObject();
		json.put("data", array);
		json.append("response", "success");

		return json.toString();
	}
	
	@Override
	public void success(JSONObject object) throws Exception{
		this.setJsonResponse(buildSuccessMsg(object));
	}
	
	private String buildSuccessMsg(JSONObject object) throws Exception{
		JSONObject json = new JSONObject();
		json.put("data", object);
		json.append("response", "success");

		return json.toString();
	}

	@Override
	public void error(Collection object) {
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		this.setJsonResponse(buildErrorMsg(object));
	}

	private String buildErrorMsg(Collection object) {
		JSONObject json = new JSONObject();
		try {
			json.accumulate("response", "fail");
			json.accumulate("data", object);

			if (object.size() > 1) {
				json.accumulate("message", "You have multiple errors. Please fix them and submit again.");
			} else {
				json.accumulate("message", "You have an error. Please fix it and submit again.");
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return json.toString();
	}

	@Override
	public void error(String msg) {
		JSONObject json = new JSONObject();
		try {
			json.accumulate("response", "fail");
			json.accumulate("data", msg);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		this.setJsonResponse(json.toString());
	}

	@Override
	public void setResponseType(String responseType) {
		this.responseType = responseType;
	}

	@Override
	public void setJsonResponse(String jsonResponse) {
		this.jsonResponse = jsonResponse;
	}

	@Override
	public void setNextNavigator(String nav) {
		nextNav = nav;
	}

	@Override
	public String getNextNavigator() {
		return nextNav;
	}
	
	@Override
	public boolean validate(ViewBean  viewbean) throws ValidationException {
		return true;
	}

	@Override
	public void setToRequest(String key, Object value) {
		request.setAttribute(key, value);		
	}

	@Override
	public void putToSession(Class t, Object object) {
		log.debug("adding object to session using key: "+t.getName());
		request.getSession().setAttribute(t.getName(), object);
	}

	@Override
	public Object getFromSession(Class t) {
		log.debug("fetching object from session using key: "+t.getName());
		return request.getSession().getAttribute(t.getName());
	}

	@Override
	public boolean userExists() {
		return request.getSession().getAttribute("concept.user") != null;
	}

	@Override
	public void bootOutUser() throws Exception {
		response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		throw new ServiceException("Unauthorized (401)");
	}
}
