/**
 * 
 */
package com.concept.mvc.navigation;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Sabelo Simelane <sabside@gmail.com>
 * 
 */
public class RequestData {
	private static final Log log = LogFactory.getLog(RequestData.class);
	
	private final HttpServletRequest request;
	private final HttpServletResponse response;
	
	public RequestData(HttpServletRequest request, HttpServletResponse response){
		this.request = request;
		this.response = response;
	}

	public void set(Object object) {
		log.info("setting to request: "+ object.getClass().getName());
		request.setAttribute(object.getClass().getSimpleName(), object);
	}

	public void set(String key, Object object) {
		log.info("setting to request: "+ key);
		request.setAttribute(key, object);
	}
}
