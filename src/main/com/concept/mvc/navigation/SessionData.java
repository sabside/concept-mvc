/**
 * 
 */
package com.concept.mvc.navigation;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.concept.mvc.navigation.controller.Scope;

/**
 * @author Sabelo Simelane <sabside@gmail.com>
 * 
 */
public class SessionData {
	
	private static final Log log = LogFactory.getLog(SessionData.class);
	
	private static final String DELIMITER = "-";
	
	private String currentCtx = "";
	private final HttpServletRequest request;
	private final HttpServletResponse response;
	
	public SessionData(HttpServletRequest request, HttpServletResponse response){
		this.request = request;
		this.response = response;
	}
	
	public void set(Object object) {
		request.getSession().setAttribute(object.getClass().getName(), object);
	}

	public void set(String key, Object object) {
		request.getSession().setAttribute(key, object);
	}
	
	public void set(Scope scope, String key, Object object) {
		String scopeStr = scope == Scope.FLOW ? currentCtx : scope.toString();
		log.info("saving object ctx: "+scopeStr + DELIMITER+ key );
		request.getSession().setAttribute(scopeStr + DELIMITER + key, object);
	}
	
	public void set(Object object, Scope scope) {
		String className = object.getClass().getName();
		
		String scopeStr = scope == Scope.FLOW ? currentCtx : scope.toString();
		log.info("saving object ctx: "+scopeStr + DELIMITER+ className );
		request.getSession().setAttribute(scopeStr + DELIMITER + className, object);
	}

	public Optional<Object> get(Class key) {
		return Optional.ofNullable(request.getSession().getAttribute(key.getName()));
	}
	
	public Optional<Object> get(String key) {
		return Optional.ofNullable(request.getSession().getAttribute(key));
	}
	
	public Optional<Object> get(Class key, Scope scope) {
		String className = key.getName();
		
		String scopeStr = scope == Scope.FLOW ? currentCtx : scope.toString();
		log.info("getting object ctx: "+scopeStr + DELIMITER + className);
		return Optional.ofNullable(request.getSession().getAttribute(scopeStr + DELIMITER + className));
	}
	
	public void destroy() {
		request.getSession().invalidate();
	}
	
	public void flowFinish(){
		currentCtx = "";
	}
	
	void setCurrentCtx(String currentCtx) {
		this.currentCtx = currentCtx;
	}
	
	String getCurrentCtx() {
		return currentCtx;
	}
}
