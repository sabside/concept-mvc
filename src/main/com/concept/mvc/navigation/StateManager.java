package com.concept.mvc.navigation;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONObject;

import com.concept.mvc.messages.MessageBundle;
import com.concept.mvc.navigation.controller.Controller;
import com.concept.mvc.navigation.controller.Navigator;
import com.concept.mvc.util.BeanUtil;
import com.concept.mvc.util.SessionValues;
import com.concept.mvc.util.StringUtil;
import com.concept.mvc.validation.ErrorMessage;
import com.concept.mvc.validation.ErrorMessages;
import com.concept.mvc.validation.Validate;
import com.concept.mvc.viewbean.ViewBean;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Stateless
@SuppressWarnings("rawtypes")
public class StateManager {
	
	private static final Log log = LogFactory.getLog(Controller.class);
	
	public static String getClass(String nav){
		String navClass = null;
		if (nav.endsWith(".nav")){
			nav = nav.replace(".nav", "");
			navClass = Controller.MAIN_NAVIGATOR_PACKAGE+ "." + nav + "Navigator";
		} else if (nav.endsWith(".mobi")){
			nav = nav.replace(".mobi", "");
			navClass = Controller.MOBILE_NAVIGATOR_PACKAGE + "." + nav + "Navigator";
		} else if (nav.endsWith(".pdf")){
			nav = nav.replace(".pdf", "");
			navClass = Controller.MAIN_NAVIGATOR_PACKAGE+ "." + nav+ "Navigator";
		}
		return navClass;
	}
	
	public static String getJSP(String nav) {
		
		if (nav.endsWith(".nav")) {
			nav = nav.replace(".nav", "");
		}
		
		if (nav.endsWith(".pdf")) {
			nav = nav.replace(".pdf", "");
		} else if (nav.endsWith(".jsp")){
			nav = nav.replace(".jsp", "");
		}
		return nav + ".jsp";
	}

	/**
	 * Takes an instance of a navigator and finds the argument viewbean and populate it with the parameter values from the request
	 * @param navigator
	 * @param req
	 * @return
	 */
	public static ViewBean getViewBean(Navigator navigator, Map<String, String> paramMap, HttpServletRequest req) {
		Method[] methods = navigator.getClass().getDeclaredMethods();
		for (int i = 0; i < methods.length; i++) {
			if (methods[i].getName().equals("execute")){
				
				Class<?>[] paramTypes = methods[i].getParameterTypes();
				try {
					Class<?> paramClass = null;
					for (int j = 0; j < paramTypes.length; j ++) {
				        paramClass = paramTypes[j];
				        if (!paramClass.isAssignableFrom(ViewBean.class)){
				        	return populateViewBean((ViewBean)paramClass.newInstance(), paramMap, req);
				        }
					}			
					
				} catch (IllegalArgumentException | InstantiationException | IllegalAccessException | InvocationTargetException  e) {
					e.printStackTrace();
				}
				
			}
		}
		return null;
	}
	
	public static ViewBean getViewBean(Navigator navigator, String jsonString) {
		Method[] methods = navigator.getClass().getDeclaredMethods();
		JSONObject json = new JSONObject(jsonString);
		String dateformat = null;
		try {
			dateformat = json.getString("dateformat");
		} catch (Exception e){}
		
		for (int i = 0; i < methods.length; i++) {
			if (methods[i].getName().equals("execute")){
				
				Class<?>[] paramTypes = methods[i].getParameterTypes();
				try {
					Class<?> paramClass = null;
					for (int j = 0; j < paramTypes.length; j ++) {
						
					    paramClass = paramTypes[j];
					    if (!paramClass.isAssignableFrom(ViewBean.class)) {
					    	
					    	Arrays.asList(paramClass.getDeclaredFields()).forEach(field->{
					    		if (field.getType().getName().equals("int")){
					    			try {
					    				String val = (String)json.get(field.getName()); 
					    				log.debug(val);
					    				if (val.isEmpty()){
					    					json.remove(field.getName());
					    				}
					    			} catch(Exception e){  				
					    			}
					    		}
					    	});
					    	
					    	Gson gson = null;
					    	if (dateformat != null){
					    		try {
					    			gson = new GsonBuilder().setDateFormat(dateformat).create();
					    		} catch(Exception e){
					    			log.error(e.getMessage(), e);
					    		}
					    	} else {
					    		gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
					    	}
							log.error("Class not assigable: "+ paramClass.getName());
							return (ViewBean) gson.fromJson(json.toString(), paramClass);
						}
					}			
					
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				}
				
			}
		}
		return null;
	}
	
	@SuppressWarnings("deprecation")
	private static ViewBean populateViewBean(ViewBean viewbean, Map<String, String> paramMap, HttpServletRequest req) throws InvocationTargetException, IllegalAccessException {
		
		
		Field[] fields = viewbean.getClass().getDeclaredFields();
		for (int i = 0; i < fields.length; i++) {
			log.debug("populating "+fields[i].getName()+"...:" +fields[i].getType());
			
			Object value = paramMap.get(fields[i].getName()) == null ? "" : paramMap.get(fields[i].getName());
			
			log.debug("object type: "+ value.getClass());
			
			if (value instanceof String){
				
				Object result = parseStringToJSON(value.toString());
				
				if (result instanceof String){
					value = ((String) value).trim();
				} else {
					value = result;
				}
			}
			
			if (fields[i].getType().toString().contains("java.sql.Timestamp")){
				value = new Timestamp(new Date().getTime());
			}  
			BeanUtils.setProperty(viewbean, fields[i].getName(), value);
			
			try {
				// Workaround!! Apache BeanUtils does not set property if the property starts with a Capital letter
				if (StringUtils.isAllUpperCase(fields[i].getName().substring(0, 1)) && !((String) value).isEmpty()) {
					BeanUtil.setProperty(viewbean, fields[i], value);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			validateField(fields[i], paramMap.get(fields[i].getName()), paramMap, req);
		}
		return viewbean;
	}
	
	/**
	 * If the string is a json array we need to parse it to a list
	 */
	private static Object parseStringToJSON(String jsonArrayString){
		if (!jsonArrayString.startsWith("[")) return jsonArrayString;
		List<Object> result = new ArrayList<Object>();
		try {
			JSONArray jsonArray = new JSONArray(jsonArrayString);
			for (int i=0; i<jsonArray.length(); i++){
				result.add(jsonArray.get(i));
			}
		} catch (Exception e){
			e.printStackTrace();
		}
		return result;
	}
	
	private static void validateField(Field field, String value, Map<String, String> paramMap, HttpServletRequest req){
		ErrorMessage message = null;
		
		if (field.isAnnotationPresent(Validate.class)){
			message = ErrorMessage.newInstance();
			
			Validate annotation = (Validate)field.getAnnotation(Validate.class);
			
			//MANDATORY
			if (annotation.mandatory()){
				if (!StringUtil.isValid(value)){
					message.setFieldId(annotation.fieldId());
					message.setFieldDisplayName(annotation.displayName());
					message.setMessage(MessageBundle.getInstance().getMessage("002") +" "+message.getFieldDisplayName());
				}
			}
			
			//DEPENDS OF
			if (!annotation.dependsOn().isEmpty() && StringUtil.isValid(paramMap.get(annotation.dependsOn()))){
				if (!StringUtil.isValid(value)){
					message.setFieldId(annotation.fieldId());
					message.setFieldDisplayName(annotation.displayName());
					message.setMessage(MessageBundle.getInstance().getMessage("002") +" "+message.getFieldDisplayName());
				}
			}
			
			//MIN LENGTH
			if (value != null && value.length() < annotation.minLength() && message.getMessage().isEmpty()){
				
				//We ignore this if the value is empty. We assume it's not mandatory
				if (!value.isEmpty()){
					message.setFieldId(annotation.fieldId());
					message.setFieldDisplayName(annotation.displayName());
					message.setMessage("The field '"+message.getFieldDisplayName()+"' is too short.");
				}
			}
			
			//MAX LENGTH
			if (value != null && value.length() > annotation.maxLength() && message.getMessage().isEmpty()){
				message.setFieldId(annotation.fieldId());
				message.setFieldDisplayName(annotation.displayName());
				message.setMessage("The field '"+message.getFieldDisplayName()+"' is too long.");
			}
		}
		
		if (message != null && !message.getMessage().isEmpty()){
			ErrorMessages messages = (ErrorMessages)req.getSession().getAttribute(SessionValues.ERROR_MESSAGES);
			if (messages == null){
				messages = new ErrorMessages();
				req.getSession().setAttribute(SessionValues.ERROR_MESSAGES, messages);
			}
			
			messages.addMessage(message.getFieldId(), message);
		}
	}
}
