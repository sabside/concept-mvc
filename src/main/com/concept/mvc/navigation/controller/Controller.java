package com.concept.mvc.navigation.controller;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Map;
import java.util.Optional;

import javax.enterprise.inject.spi.BeanManager;
import javax.inject.Inject;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONException;
import org.json.JSONObject;

import com.concept.dbtools.DBAccessor;
import com.concept.dbtools.jdbc.ConfigFactory;
import com.concept.mvc.cache.SessionBean;
import com.concept.mvc.cache.SessionBeanCleaner;
import com.concept.mvc.cache.SessionBeanManager;
import com.concept.mvc.navigation.NavigatorUtil;
import com.concept.mvc.navigation.NavigatorUtilImpl;
import com.concept.mvc.navigation.StateManager;
import com.concept.mvc.persistence.DatabaseAccessor;
import com.concept.mvc.reporting.Reporter;
import com.concept.mvc.reporting.Severity;
import com.concept.mvc.util.Global;
import com.concept.mvc.util.PropertiesManager;
import com.concept.mvc.util.ReflectionUtil;
import com.concept.mvc.util.SessionValues;
import com.concept.mvc.validation.ErrorMessages;
import com.concept.mvc.version.Version;
import com.concept.mvc.viewbean.ViewBean;
import com.concept.utils.CDIUtil;

/**
 * 
 * @author Sabside
 *
 */
@SuppressWarnings("rawtypes")
@WebServlet(name = "Controller", urlPatterns = "/Controller", loadOnStartup = 1)
public class Controller extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final Log log = LogFactory.getLog(Controller.class);

	public static boolean TESTING = false;

	public final static String JSON_TYPE = "application/json;charset=UTF-8";
	public static String serverPath;
	public static String APP_VERSION;

	public static String MAIN_PACKAGE;
	public static String MAIN_NAVIGATOR_PACKAGE;
	public static String MOBILE_NAVIGATOR_PACKAGE;
	public static String DATASOURCE;
	public static String FILE_SYSTEM_DB_FILENAME;
	public static String INITIALIZER_CLASS;
	public static String DB_CONNECTION_DETAILS;
	
	@Inject CDIUtil cdiUtil;
	NavigatorUtil navUtil;

	@Inject Initializer initializer;
	@Inject DBAccessor dbAccessor;
	@Inject BeanManager bm;
	@Inject NavigationService navigationService;
	
	private String processPath(String path) {
		log.info("current path: " + path);
		if (path.contains("WEB-INF")) {
			log.info("we are replacing W`EB-INF...");
			return path.replace("WEB-INF", "");
		}
		return path;
	}

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		serverPath = processPath(config.getServletContext().getRealPath("/WEB-INF/"));
		log.info("serverpath : " + serverPath);
		INITIALIZER_CLASS = config.getServletContext().getInitParameter("initializer.class");
		MAIN_PACKAGE = config.getServletContext().getInitParameter("main.package");
		MAIN_NAVIGATOR_PACKAGE = config.getServletContext().getInitParameter("main.navigator.package");
		MOBILE_NAVIGATOR_PACKAGE = config.getServletContext().getInitParameter("mobile.navigator.package");

		DB_CONNECTION_DETAILS = config.getServletContext().getInitParameter("connection.details");
		dbAccessor.setup(ConfigFactory.createConfig(DB_CONNECTION_DETAILS, dbAccessor));

		if (DB_CONNECTION_DETAILS == null) log.warn("Connection details have not been set in the web.xml.");
			
		log.info("connectionDetails: "+ DB_CONNECTION_DETAILS);

		FILE_SYSTEM_DB_FILENAME = config.getServletContext().getInitParameter("filesystem.persistence.file");

		DatabaseAccessor.TESTING = config.getServletContext().getInitParameter("testing") == null ? false : Boolean.parseBoolean(config.getServletContext().getInitParameter("testing"));

		com.concept.mvc.util.PropertiesManager.getInstance().setDataPath(Controller.serverPath + "/WEB-INF/datafiles");

		APP_VERSION = Version.getVersionNumber();
		try {
			initializer.initialize();
		} catch (Exception e) {
			e.printStackTrace();
			throw new ServletException(e);
		}
		new SessionBeanCleaner().start();
	}

	@SuppressWarnings({ "unchecked" })
	public void doGet(HttpServletRequest req, HttpServletResponse resp) {
		try {

			String sessionId = req.getSession().getId();
			if (!SessionBeanManager.getInstance().get(sessionId).isPresent()) {
				log.info(String.format("session [%s] does not exist...", sessionId));
				SessionBeanManager.getInstance().put(sessionId, new SessionBean());
				log.info("user agent: "+ req.getHeader("user-agent"));
			} else {
				SessionBeanManager.getInstance().get(sessionId).get().renew();
			}
			
			if (Global.serverName == null){
				int port = req.getServerPort();
				String strPort = (port == 80 ? "" : ":" + String.valueOf(port));
				Global.serverName = req.getServerName()+strPort;
				log.info("servername: "+ Global.serverName);
			}
			
			if (req.getParameter("redirect") != null && req.getParameter("redirect").equals("true")){
				
				req.removeAttribute("redirect");
				req.setAttribute("redirect", null);
				req.setAttribute("nav", req.getParameter("nav"));
				
				JSONObject parm = new JSONObject();
				Enumeration<String> parameterNames = req.getParameterNames();
				while (parameterNames.hasMoreElements()) {
					String paramName = parameterNames.nextElement();
					String[] paramValues = req.getParameterValues(paramName);
					for (int i = 0; i < paramValues.length; i++) {
						String paramValue = paramValues[i];
						req.setAttribute(paramName, paramValue);
					}
				}
				
				req.setAttribute("parms", parm);
				String jsp = req.getParameter("page") == null ? "main.jsp" : req.getParameter("page");
				if (!jsp.endsWith("jsp")) {
					jsp = jsp.concat(".jsp");
				}
				log.info("I am redirecting... "+jsp);
				req.getRequestDispatcher("/".concat(jsp)).forward(req, resp);
				return;
			}
			
			log.info("in doGet...:" + (req.getHeader("X-Forwarded-For") == null ? req.getRemoteAddr() : req.getHeader("X-Forwarded-For")));

			long before = System.currentTimeMillis();

			String responseJson = null;
			printParams(req);
			String navParm = null;
			JSONObject json = null;
			Map<String, String> paramMap = null;

			if (req.getHeader("Content-Type") != null && req.getHeader("Content-Type").contains("application/json")) {
				String jsonString = req.getAttribute("json") == null ? null : req.getAttribute("json").toString();
				if (jsonString == null){
					jsonString = navigationService.getJSONString(req);
				}
				
				json = new JSONObject(jsonString);
				navParm = json.getString("nav");
			} else {
				paramMap = navigationService.getParameterMap(req);
				navParm = navigationService.getNavParm(req, paramMap);
			}

			// We clear error messages
			ErrorMessages errors = (ErrorMessages) req.getSession().getAttribute(SessionValues.ERROR_MESSAGES);

			Navigator nav = null;
			try {
				nav = (Navigator) cdiUtil.newInstance(Navigator.class, StateManager.getClass(navParm));
				
				if (nav != null) {
					
					navUtil = (NavigatorUtil) cdiUtil.newInstance(NavigatorUtil.class, NavigatorUtilImpl.class.getName());
					navUtil.initialize(req, resp);

					if (req.getHeader("dataType") != null) {
						ReturnType responseType = ReturnType.valueOf(StringUtils.upperCase(req.getHeader("dataType")));
						if (responseType != null) {
							navUtil.setResponseType(responseType.name());
						}
					}
					
					if (req.getHeader("dateformat") != null) {
						json.put("dateformat", req.getHeader("dateformat"));
					}
					
				}
				
			} catch (Exception e) {
				e.printStackTrace();
				log.error("could not find Navigator");
			}
	
			// If the navigator doesn't exist we need to return the JSP. It's not a train smash
			if (nav != null) {

				ViewBean viewbean = null;
				if (json == null) {
					viewbean = StateManager.getViewBean(nav, paramMap, req);
				} else {
					viewbean = StateManager.getViewBean(nav, json.toString());
				}

				errors = (ErrorMessages) req.getSession().getAttribute(SessionValues.ERROR_MESSAGES);
				boolean validation = true;

				// We only wan call the Navigator Validate Method only if there were no Viewbean validation errors
				if (errors == null || errors.getMessages().size() == 0) {
					validation = navUtil.validate(viewbean);
				}

				errors = (ErrorMessages) req.getSession().getAttribute(SessionValues.ERROR_MESSAGES);

				if (errors != null && errors.getMessages().size() > 0) {
					// We come in here only if the submitted parameters failed validations according to the viewbeans defined
					navUtil.error(ErrorMessages.toList(errors.getMessages()));
					responseJson = navUtil.getJSONResponse();
					navUtil.setJsonResponse(responseJson);
					navUtil.setResponseType(NavigatorUtilImpl.JSON_RESPONSE);
					resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
					handleJSONResponse(req, resp, nav);
				} else if (!validation) {
					// There's problem here! Validation failed but there is not set error message.
					navUtil.error("An error occured. Please try again.");
					navUtil.setJsonResponse(responseJson);
					navUtil.setResponseType(NavigatorUtilImpl.JSON_RESPONSE);
					log.error("There's problem here! Validation failed but there is not set error message");
					new Reporter().report(Severity.CRITICAL, "There's problem here! Validation failed but there is not set error message. Look for this line in the logs and fix it!!!");
					resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				} else {
					// This is where the magic happens. This executes the target navigator
					log.info("invoking navigator: " + nav.getClass().getName());
					nav.execute(viewbean);
					
					errors = (ErrorMessages) req.getSession().getAttribute(SessionValues.ERROR_MESSAGES);
					if (errors != null && errors.getMessages().size() > 0) {
						// We come in here only if the submitted parameters failed validations according to the viewbeans defined
						navUtil.error(ErrorMessages.toList(errors.getMessages()));
						responseJson = navUtil.getJSONResponse();
						navUtil.setJsonResponse(responseJson);
						navUtil.setResponseType(NavigatorUtilImpl.JSON_RESPONSE);
						resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
					}
				}

				// Sometimes a navigator may delegate to another. We call the doGet method again.
				if (navUtil.getNextNavigator() != null) {
					log.info("getting next nav=" + navUtil.getNextNavigator());
					req.setAttribute("nav", navUtil.getNextNavigator());
					doGet(req, resp);
					return;
				}

				// response type according to the headers
				String returnType = req.getHeader("returnType") == null ? "" : req.getHeader("returnType");
				if (!returnType.isEmpty() && returnType.equals(NavigatorUtilImpl.JSON_RESPONSE)) {
					handleJSONResponse(req, resp, nav);
				} else if (!returnType.isEmpty() && returnType.equals(NavigatorUtilImpl.JSP_RESPONSE) && req.getHeader("redirect") != null) {
					handleJSPResponse(req, resp, req.getHeader("redirect"));

				} else if (returnType.isEmpty() && (navUtil.getResponseType().equals(NavigatorUtilImpl.JSON_RESPONSE) || isReturnType(nav, ReturnType.JSON))) { // We setup the response to the client
					log.warn("returning json response...." + navUtil.getJSONResponse());
					handleJSONResponse(req, resp, nav);
				} else if (returnType.isEmpty() && navUtil.getResponseType().equals(NavigatorUtilImpl.PDF_RESPONSE)) {
					log.info("PDF response...");
				} else if (returnType.equals(NavigatorUtilImpl.JSP_RESPONSE) || navUtil.getResponseType().equals(NavigatorUtilImpl.JSP_RESPONSE) || isReturnType(nav, ReturnType.JSP)) {
					
					if (getJSP(nav).isPresent()) {
						resp.setHeader("jspname", getJSP(nav).get());
					}
					handleJSPResponse(req, resp, navParm);
				}
			} else {
				log.info("Navigator doesn't exist, returning JSP instead");
				String jspName = StateManager.getJSP(navParm);
				log.info("going to jsp: " + jspName);
				req.getRequestDispatcher("/" + jspName).forward(req, resp);
				return;
			}

			// We clear errors. Is this the right place???
			if (errors != null)
				errors.getMessages().clear();

			long after = System.currentTimeMillis();
			log.info("... duration: " + (after - before));

		} catch (Exception e) {
			e.printStackTrace();
			log.error("ERROR IN CONTROLLER");
			handleControllerError(req, resp, e);
		}
	}

	private void handleJSONResponse(HttpServletRequest req, HttpServletResponse resp, Navigator nav) throws IOException {
		//log.warn("returning json response...." + navUtil.getJSONResponse());
		resp.setContentType(JSON_TYPE);
		addNoCacheHeaders(resp);
		
		if (navUtil.getJSONResponse() == null) throw new IOException("You did not return a response from your navigator and the servlet is expecting a json response!");
			
		resp.getOutputStream().write(navUtil.getJSONResponse().getBytes());
		resp.getOutputStream().flush();
	}

	private void handleJSPResponse(HttpServletRequest req, HttpServletResponse resp, String navParm) throws IOException, ServletException {
		if (resp.getHeader("jspname") != null) {
			String jspName = resp.getHeader("jspname");
			if (jspName.startsWith("/")) {
				jspName = jspName.substring(1);
			}
			
			log.info("going to jsp: " + jspName);
			req.getRequestDispatcher("/" + jspName).forward(req, resp);
			
		} else { 
			String jspName = StateManager.getJSP(navParm);
			log.info("going to jsp: " + jspName);
			req.getRequestDispatcher("/jsp/" + jspName).forward(req, resp);
		}
	}

	private void handleControllerError(HttpServletRequest req, HttpServletResponse resp, Exception e) {
		resp.setContentType(JSON_TYPE);
		addNoCacheHeaders(resp);

		String errorMsg = "Error in application!";
		if (e.getMessage().contains("java.lang.NumberFormatException")){
			errorMsg = "This field can only be numbers.";
		}

		if (Boolean.parseBoolean((String) PropertiesManager.getInstance().getSystemProperties().setProperty("enableTesting", "false"))) {
			errorMsg = e.getMessage();
		}

		JSONObject json = null;
		JSONObject error = new JSONObject();
		try {
			error.accumulate("message", e.getMessage() == null ? "An error occured, please try again." : e.getMessage());
			json = new JSONObject();
			json.accumulate("response", HttpServletResponse.SC_BAD_REQUEST);
			json.accumulate("data", error);
			json.accumulate("message", errorMsg);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}

		log.error("Error in Controller: " + json.toString());

		try {
			resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			resp.getOutputStream().write(json.toString().getBytes());
			resp.getOutputStream().flush();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	public void doPost(HttpServletRequest req, HttpServletResponse resp) {
		doGet(req, resp);
	}

	private void printParams(HttpServletRequest req) {
		Enumeration<String> parameterNames = req.getParameterNames();
		while (parameterNames.hasMoreElements()) {
			String paramName = parameterNames.nextElement();
			String[] paramValues = req.getParameterValues(paramName);
			for (int i = 0; i < paramValues.length; i++) {
				String paramValue = paramValues[i];
				if (paramName.equals("password")) {
					log.info(paramName + " : **********");
				} else {
					log.info(paramName + " : " + paramValue);
				}
			}
		}
	}

	private void addNoCacheHeaders(HttpServletResponse response) {
		response.addDateHeader("Expires", 0);
		response.addHeader("Pragma", "nocache");
		response.addHeader("Cache-Control", "nocache");
	}

	private Optional<String> getJSP(Object navigator) {
		com.concept.mvc.navigation.controller.Navigate navigatorAnnotation = (com.concept.mvc.navigation.controller.Navigate) ReflectionUtil.getAnnotationFromClass(navigator, com.concept.mvc.navigation.controller.Navigate.class);
		if (navigatorAnnotation != null && navigatorAnnotation.jsp() != null) {
			return Optional.ofNullable(navigatorAnnotation.jsp());
		}
		return Optional.empty();
	}
	
	private boolean isReturnType(Object navigator, ReturnType returnType) {
		com.concept.mvc.navigation.controller.Navigate navigatorAnnotation = (com.concept.mvc.navigation.controller.Navigate) ReflectionUtil.getAnnotationFromClass(navigator, com.concept.mvc.navigation.controller.Navigate.class);
		if (navigatorAnnotation != null) {
			return navigatorAnnotation.returnType() == returnType;
		}
		return false;
	}
}