package com.concept.mvc.navigation.controller;

public interface Initializer {

	public void initialize() throws Exception;
}
