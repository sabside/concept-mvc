/**
 * 
 */
package com.concept.mvc.navigation.controller;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.concept.mvc.bpm.dto.User;

/**
 * @author Sabside
 * 
 * You can override the return type defined here by passing a header with key "returnType" e.g. {returnType: 'JSON'} OR you can also pass JSP.
 * This will override the return type defined in the annotation.
 */
@Documented
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface Navigate {

	ReturnType returnType() default ReturnType.JSP;

	String jsp() default "";
	
	Class<? extends Flow> flow() default DefaultFlow.class;
	
	@SuppressWarnings("rawtypes")
	Class[] objects() default User.class;
}
