package com.concept.mvc.navigation.controller;

import java.io.BufferedReader;
import java.util.Base64;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;

import com.concept.mvc.authentication.dto.Credentials;
import com.concept.mvc.util.IPUtils;
import com.concept.mvc.util.SessionValues;
import com.concept.mvc.validation.ErrorMessage;
import com.concept.mvc.validation.ErrorMessages;

@Stateless
public class NavigationService {

	private static final Log log = LogFactory.getLog(NavigationService.class);

	public String getNavParm(HttpServletRequest req, Map<String, String> paramMap) throws Exception {
		String navParm = paramMap.get("nav");
		
		String jsonString = paramMap.get("json");
		if (jsonString != null){
			JSONObject json = new JSONObject(jsonString);
			return json.get("nav").toString();
		}
		
		navParm = paramMap.get("nav") == null ? null : paramMap.get("nav");

		if (navParm == null || navParm.isEmpty())
			throw new Exception("nav parm is null or empty!!!");

		if (req.getAttribute("nav") != null) {
			navParm = req.getAttribute("nav").toString();
			log.info("redirected navParm to " + navParm);
		}

		return navParm;
	}

	public String getJSONString(HttpServletRequest req) throws Exception {
		if (req.getHeader("Content-Type") != null && req.getHeader("Content-Type").contains("application/json")) {
			StringBuffer sb = new StringBuffer();
			BufferedReader reader = req.getReader();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
			
			String copy = sb.toString();
			JSONObject json = new JSONObject(copy);
			if (json.has("password")){
				json.put("password", "***********");
			}				
			
			log.info(json.toString());
			return sb.toString();
		}
		throw new Exception("Content is not json");
	}
	
	public Map<String, String> getParameterMap(HttpServletRequest req) throws Exception {
		Map<String, String> paramMap = null;
		String jsonString = null;

		if (req.getHeader("Content-Type") != null && req.getHeader("Content-Type").contains("application/json")) {
			log.info("received a json request...");
		} else {
			paramMap = convertMap(req.getParameterMap());
		}

		paramMap.put("jsonString", jsonString); // HACK!!!

		return paramMap;
	}

	private Map<String, String> convertMap(Map<String, String[]> input) {
		Map<String, String> result = new HashMap<String, String>();
		for (Iterator iterator = input.keySet().iterator(); iterator.hasNext();) {
			String key = (String) iterator.next();
			result.put(key, input.get(key)[0]);
		}
		return result;
	}

	public void registerErrorToSession(HttpServletRequest req, String msg) {
		ErrorMessages messages = (ErrorMessages) req.getSession().getAttribute(SessionValues.ERROR_MESSAGES);
		if (messages == null) {
			messages = new ErrorMessages();
			req.getSession().setAttribute(SessionValues.ERROR_MESSAGES, messages);
		}
		ErrorMessage message = ErrorMessage.newInstance();
		message.setFieldDisplayName("");
		message.setFieldId("");
		message.setMessage(msg);
		messages.addMessage(message.getFieldId(), message);
	}

	@SuppressWarnings("restriction")
	public Credentials breakDownCredentials(String auth) throws Exception {

		if (auth == null) {
			return null; // no auth
		}
		if (!auth.toUpperCase().startsWith("BASIC ")) {
			return null; // we only do BASIC
		}
		// Get encoded user and password, comes after "BASIC "
		String userpassEncoded = auth.substring(6);
		// Decode it, using any base 64 decoder

		String userpassDecoded = new String(Base64.getDecoder().decode(userpassEncoded));

		String[] constructs = userpassDecoded.split(":");

		log.info("username: " + constructs[0]);

		return new Credentials(constructs[0], constructs[1]);
	}

	public boolean isIPAllowed(HttpServletRequest request) {
		return IPUtils.isIPv4Private(IPUtils.getIpFromRequest(request));
	}
}
