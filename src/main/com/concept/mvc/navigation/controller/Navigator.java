package com.concept.mvc.navigation.controller;

import javax.ejb.Stateless;

import com.concept.mvc.navigation.NavigatorException;
import com.concept.mvc.viewbean.ViewBean;

/**
 * 
 * @author f3557790
 * @param <ViewBean>
 * @param <ViewBean>
 *
 */
@Stateless
public interface Navigator<T extends ViewBean> {
	
	public void execute(T viewbean) throws NavigatorException;
}
