package com.concept.mvc.navigation.controller;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.concept.mvc.NavigatorNotFoundException;


public class NavigatorFactory {

	
	@SuppressWarnings("rawtypes")
	private Map<Class, Object> dependencyMap = null;
	private Class newInstance = null;
	
	public NavigatorFactory(Class newInstance, Map<Class, Object> dependencyMap) {
		this.newInstance = newInstance;

		this.dependencyMap = new HashMap<>();
		dependencyMap.forEach((k, v) -> {
			this.dependencyMap.put(k, v);
		});

	}
	
	public Navigator makeNavigator() throws InstantiationException, IllegalAccessException, NoSuchMethodException, SecurityException, IllegalArgumentException, InvocationTargetException, NavigatorNotFoundException {

		return Arrays.asList(newInstance.getConstructors()).stream()
				// Sort by number of parms - we want the most targetted
				// constructor we can find
				.sorted((first, second) -> {
					return Integer.compare(first.getParameterCount(), second.getParameterCount());
				})
				// Filter out constructors that we don't have matching
				// dependencies for
				.filter(c -> {
					return Arrays.asList(c.getParameterTypes()).stream().allMatch(type -> {
						return dependencyMap.containsKey(type);
					});
				})
				// Select the first candidate
				.findFirst()
				// Map the constructor to a concrete instance
				.map(c -> {
					try {

						List<Object> parmObjects = new ArrayList<>();
						Arrays.asList(c.getParameterTypes()).stream().forEach(type -> {
							Object param = dependencyMap.get(type);
							
							parmObjects.add(param);
						});

						
						
						return (Navigator) c.newInstance(parmObjects.toArray());

					} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
						e.printStackTrace();
					}
					return null;
				}).orElseThrow(NavigatorNotFoundException::new);

	}
}
