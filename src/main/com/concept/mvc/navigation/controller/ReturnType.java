/**
 * 
 */
package com.concept.mvc.navigation.controller;

/**
 * @author Sabside
 * 
 */
public enum ReturnType {
	JSP, JSON;
}
