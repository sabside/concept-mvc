/**
 * 
 */
package com.concept.mvc.navigation.controller;

/**
 * @author F3557790
 * 
 */
public enum Scope {

	SESSION, FLOW, REQUEST
}
