package com.concept.mvc.navigation.controller;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.Map;

import javax.enterprise.inject.spi.Bean;
import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;

import com.concept.mvc.navigation.NavigatorUtil;
import com.concept.mvc.navigation.NavigatorUtilImpl;
import com.concept.mvc.navigation.Prelogin;
import com.concept.mvc.navigation.StateManager;
import com.concept.mvc.util.ReflectionUtil;
import com.concept.mvc.viewbean.ViewBean;
import com.concept.utils.CDIUtil;

@WebFilter("/Controller")
/**
 * Will not allow user to invoke any navigator unless the navigator is annotated with @Prelogin or a user exists in the session.
 * To check if the user exists, we check in the session a key "concept.user" if it's null. If it's not null we assume the user is logged in.
 * After login, use setUser(UserObject) in the navigator so that we can pick up the user and regard the user as authenticated.
 * 
 * @author Sabelo Simelane [Sabelo Simelane <sabside@gmail.com>]
 *
 */
public class SecurityFilter extends HttpServlet implements Filter{

	private static final Log log = LogFactory.getLog(SecurityFilter.class);
	
	private static final long serialVersionUID = 1L;
	private ServletContext context;
	NavigationService navigationService = new NavigationService();
	private static final String CONCEPT_USER = "concept.user";

	NavigatorUtil navUtil;
	@Inject CDIUtil cdiUtil;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;

		try {
			
			String navParm = null;
			if (req.getHeader("Content-Type") != null && req.getHeader("Content-Type").contains("application/json")) {
				String jsonString = navigationService.getJSONString(req);
				JSONObject json = new JSONObject(jsonString);
				navParm = json.getString("nav");
				req.setAttribute("json", jsonString);
			} else {
				Map<String, String> paramMap = navigationService.getParameterMap(req);
				navParm = navigationService.getNavParm(req, paramMap);
			}
			
			Navigator nav = null;
			
			try {
				nav = (Navigator) cdiUtil.newInstance(Navigator.class, StateManager.getClass(navParm));
				
				navUtil = (NavigatorUtil) cdiUtil.newInstance(NavigatorUtilImpl.class, NavigatorUtilImpl.class.getName());
				navUtil.initialize(req, res);
				log.debug("class initiated successfully..."+ StateManager.getClass(navParm));
			} catch (ClassNotFoundException ex) {
				// It's normal to get this one. It simply means we will return a corresponding JSP.
				chain.doFilter(request, response);
				return;
			}

			Annotation annotation = ReflectionUtil.getAnnotationFromClass(nav, Prelogin.class);
			log.debug("annotation is null? : " +(annotation == null));
			
			if (annotation == null && req.getSession().getAttribute(CONCEPT_USER) == null) {
				
				log.error("unauthorized user! You can annotate the Navigator with @Prelogin if you want to invoke it outside of login.");
				res.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
				res.getOutputStream().write("Unauthorized (401)".getBytes());
				res.getOutputStream().flush();
				
			} else {
				chain.doFilter(request, response);
			}
			
		} catch (Exception e){
			e.printStackTrace();
			RequestDispatcher requestDispatcher; 
			requestDispatcher = request.getRequestDispatcher("/");
			requestDispatcher.forward(request, response);
		}
	}

	@Override
	public void init(FilterConfig fConfig) throws ServletException {
		this.context = fConfig.getServletContext();
	}
}
