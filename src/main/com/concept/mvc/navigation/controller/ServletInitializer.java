/**
 * 
 */
package com.concept.mvc.navigation.controller;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.concept.mvc.persistence.DatabaseAccessor;

/**
 * @author Sabside
 * 
 */

public class ServletInitializer implements ServletContextListener {

	private static final Log log = LogFactory.getLog(ServletInitializer.class);

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		log.info("initializing context...");
		
		String DATASOURCE = sce.getServletContext().getInitParameter("datasource");
		DatabaseAccessor.DS_NAME = DATASOURCE;

	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		// TODO Auto-generated method stub

	}

}
