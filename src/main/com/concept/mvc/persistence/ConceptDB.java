package com.concept.mvc.persistence;

/**
 * Custom key/value persistance Framework
 * 
 * @author Sabside
 *
 */
public interface ConceptDB {

	public void initializeConceptDB(String filename) throws ConceptDBException;

	public Object retrieve(String tablename, String key) throws ConceptDBException;

	public void persist(String tablename, String key, Object value) throws ConceptDBException;

	public void delete(String tablename, String key) throws ConceptDBException;

	public void update(String tablename, String key, Object value) throws ConceptDBException;
}
