package com.concept.mvc.persistence;

public class ConceptDBException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ConceptDBException() {
	}

	public ConceptDBException(String message) {
		super(message);
	}

	public ConceptDBException(Throwable cause) {
		super(cause);
	}

	public ConceptDBException(String message, Throwable cause) {
		super(message, cause);
	}

	public ConceptDBException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
