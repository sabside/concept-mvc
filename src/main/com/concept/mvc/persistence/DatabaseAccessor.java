package com.concept.mvc.persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * 
 * @author F3557790
 *
 */
public abstract class DatabaseAccessor {

	public static Boolean TESTING = Boolean.FALSE;
	public static String DS_NAME;
	
	protected Connection connect() throws SQLException {

		if (DS_NAME == null) {
			throw new SQLException("Datasource has not been set. Set a context-param (datasource) in web.xml ");
		}

		Connection conn = null;
		try {
			Context envCtx = new InitialContext();

			DataSource ds = (DataSource) envCtx.lookup(DS_NAME);
			conn = ds.getConnection();

		} catch (NamingException e) {
			// Is testing enabled
			if (!TESTING) {
				e.printStackTrace();
				throw new SQLException(e);
			}

			// If testing is enabled we get the connection out of the container context
			conn = new TestingDatabaseAccessor(DS_NAME).getTestingConnection();
		}

		return conn;
	}

	protected void disconnect(Connection connection, PreparedStatement query) {
		DatabaseUtil.close(connection, query);
	}

	protected void disconnect(Connection connection) {
		DatabaseUtil.close(connection, null);
	}
}
