package com.concept.mvc.persistence;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;

/**
 * 
 * @author F3557790
 *
 */
public class TestingDatabaseAccessor {

	private final String dsFilename;

	public TestingDatabaseAccessor(String dsFilename){
		this.dsFilename = dsFilename;	
	}
	
	public Connection getTestingConnection() {
		Connection conn = null;
		try {
			
			String fileContents = new String(Files.readAllBytes(Paths.get(dsFilename)));
			JSONObject dsFile = XML.toJSONObject(fileContents);
			JSONObject json = dsFile.getJSONObject("datasources");
			JSONArray array = json.getJSONArray("datasource");
			
			JSONObject ds = null;
			
			//what datasource must we use??
			String dsName = DatabaseAccessor.DS_NAME;
			
			for (int i=0; i<array.length(); i++){
				String jndiName = ((JSONObject) array.get(i)).getString("jndi-name");
				if (jndiName.equals(dsName)){
					ds = (JSONObject) array.get(i);
					break;
				}
			}
			
			String connectionString = ds.getString("connection-url");

			String username = ds.getJSONObject("security").getString("user-name");
			String password = ds.getJSONObject("security").getString("password");

			DriverManager.registerDriver((Driver) Class.forName("org.postgresql.Driver").newInstance());
			conn = DriverManager.getConnection(connectionString, username, password);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return conn;
	}
}
