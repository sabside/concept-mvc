package com.concept.mvc.properties;

import java.util.Properties;

public interface PropertiesDAO {

	Properties fetch(String filename) throws Exception;
}
