package com.concept.mvc.redirect;

public interface Redirect {

	String handleRedirect(RedirectMetaData metadata) throws Exception;
	
	/**
	 * Must the navigator invalidate the token or will you do it yourself?
	 * @return
	 */
	boolean isInvalidateToken();
}
