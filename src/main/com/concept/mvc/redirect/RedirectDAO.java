package com.concept.mvc.redirect;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Optional;

import javax.inject.Inject;

import com.concept.dbtools.DBAccessor;

public class RedirectDAO {

	private @Inject DBAccessor dbAccessor;

	public void invalidateToken(String token) throws Exception {
		Connection conn = null;
		try {
			conn = dbAccessor.connect();
			PreparedStatement stmt = conn.prepareStatement("update redirects set used=true where token = ?");
			stmt.setString(1, token);
			stmt.executeUpdate();

		} finally {
			dbAccessor.disconnect(conn);
		}
	}
	
	public Optional<RedirectToken> fetch(String token, int expiryInHours) throws Exception {
		Connection conn = null;
		try {
			conn = dbAccessor.connect();
			PreparedStatement stmt = conn.prepareStatement("select * from redirects where token = ? and created > ? and used is false");
			stmt.setString(1, token);
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.HOUR, -1 * expiryInHours);
			
			stmt.setTimestamp(2, new Timestamp(cal.getTimeInMillis()));
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				return Optional.ofNullable(new RedirectToken(rs.getString("token"), rs.getString("handler"), RedirectToken.decodeMetadata(rs.getString("context"))));
			}

		} finally {
			dbAccessor.disconnect(conn);
		}
		return Optional.empty();
	}
	
	public void save(RedirectToken token) throws Exception {
		setup();	
		Connection conn = null;
		try {
			conn = dbAccessor.connect();
			PreparedStatement stmt = conn.prepareStatement("insert into redirects (token, handler, created, context) values (?, ?, now(), ?)");
			stmt.setString(1, token.getToken());
			stmt.setString(2, token.getHandlerClass());
			stmt.setString(3, token.encodeMetadata());
			stmt.executeUpdate();

		} finally {
			dbAccessor.disconnect(conn);
		}
	}

	public void setup() throws Exception {
		createTable();
	}

	private void createTable() throws Exception {
		Connection conn = null;
		try {
			conn = dbAccessor.connect();
			PreparedStatement stmt = conn.prepareStatement("create table if not exists redirects (token text not null, handler text not null, "
					+ "created timestamp with time zone not null, used boolean not null default false, context text)");
			stmt.execute();

		} finally {
			dbAccessor.disconnect(conn);
		}
	}

}
