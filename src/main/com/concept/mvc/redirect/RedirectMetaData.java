package com.concept.mvc.redirect;

public class RedirectMetaData {

	private final String resultsPage;
	private final String successMessage;
	private final String failMessage;
	private final String context;

	public RedirectMetaData(String resultsPage, String successMessage, String failMessage, String context) {
		super();
		this.resultsPage = resultsPage;
		this.successMessage = successMessage;
		this.failMessage = failMessage;
		this.context = context;
	}

	public String getResultsPage() {
		return resultsPage;
	}

	public String getSuccessMessage() {
		return successMessage;
	}

	public String getFailMessage() {
		return failMessage;
	}

	public String getContext() {
		return context;
	}

}
