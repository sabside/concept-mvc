package com.concept.mvc.redirect;

import java.util.Optional;
import java.util.UUID;

import javax.inject.Inject;

public class RedirectService {
	private static final int TOKEN_EXPIRY_IN_HOURS = 72;
	private @Inject RedirectDAO dao;
	
	public String createToken() {
		return  UUID.randomUUID().toString().replace("-", "");
	}
	
	public void save(Class<? extends Redirect> redirect, String token, RedirectMetaData metadata) throws Exception {
		dao.save(new RedirectToken(token, redirect.getName(), metadata));
	}
	
	/**
	 * Looks up the token in the DB that is not expired.
	 * @param token
	 * 			Token to look for.
	 * @param expiryInHours	
	 * 			number of hours a token is considered expired. 
	 * @return
	 * @throws Exception
	 */
	public Optional<RedirectToken> fetch(String token) throws Exception {
		return dao.fetch(token, TOKEN_EXPIRY_IN_HOURS);
	}
	
	public void invalidateToken(String token) throws Exception {
		dao.invalidateToken(token);
	}
	
	public String createLink(String host, String token) {
		return String.format("%s/Snippet/Controller?nav=Redirect.nav&stringValue=%s", host, token);
	}
}
