package com.concept.mvc.redirect;

import java.util.Base64;

import org.json.JSONObject;

import com.google.gson.Gson;

public class RedirectToken {

	private final String token;
	private final String handlerClass;
	private final RedirectMetaData metadata;

	public RedirectToken(String token, String handlerClass, RedirectMetaData metadata) {
		super();
		this.token = token;
		this.handlerClass = handlerClass;
		this.metadata = metadata;
	}

	public String getToken() {
		return token;
	}

	public String getHandlerClass() {
		return handlerClass;
	}

	public RedirectMetaData getMetadata() {
		return metadata;
	}

	public String encodeMetadata() {
		JSONObject json = new JSONObject(metadata);
		return Base64.getEncoder().encodeToString(json.toString().getBytes());
	}
	
	public static RedirectMetaData decodeMetadata(String hash) {
		String decoded = new String(Base64.getDecoder().decode(hash));
		return new Gson().fromJson(decoded, RedirectMetaData.class);
	}
}
