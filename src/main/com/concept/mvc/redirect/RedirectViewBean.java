package com.concept.mvc.redirect;

import java.util.Base64;

import org.json.JSONObject;

import com.concept.mvc.viewbean.ViewBean;

public class RedirectViewBean implements ViewBean {

	private final String token;
	private final String resultPage;
	
	public RedirectViewBean(String token, String resultPage) {
		this.token = token;
		this.resultPage = resultPage;
	}

	public String getToken() {
		return token;
	}

	public String getResultPage() {
		return resultPage;
	}

	public String encode() {
		return Base64.getEncoder().encodeToString(new JSONObject(this).toString().getBytes());
	}
}
