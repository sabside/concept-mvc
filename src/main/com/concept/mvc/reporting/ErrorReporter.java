package com.concept.mvc.reporting;

public interface ErrorReporter {

	void report(Severity severity, Exception e, String message);
	
	void report(Severity severity, String message);
	
	void report(Severity severity, String subject, String message);

	void report(Severity severity, String subject, String message, byte[] attachment,String recipients);
}
