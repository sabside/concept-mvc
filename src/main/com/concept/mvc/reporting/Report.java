package com.concept.mvc.reporting;

public class Report {

	Severity eventType;

	public Report(Severity eventType) {
		super();
		this.eventType = eventType;
	}

	public Severity getEventType() {
		return eventType;
	}

}
