/**
 * 
 */
package com.concept.mvc.reporting;

import javax.inject.Inject;

/**
 * @author Sabside
 *
 */
@Deprecated
public class Reporter {

	@Inject ErrorReporter reporter;
	
	public void report(Severity severity, Exception e, String message) {

		new Thread(() -> {
			reporter.report(severity, e, message);
		}).start();
	}

	public void report(Severity severity, String message) {

		new Thread(() -> {
			reporter.report(severity, message);
		}).start();
	}
	
	public void report(Severity severity, String subject, String message) {

		new Thread(() -> {
			reporter.report(severity, subject, message);
		}).start();
	}

	public void report(Severity severity, String subject, String message, byte [] attachment,String recipients) {

		new Thread(() -> {
			reporter.report(severity, subject, message,attachment,recipients);
		}).start();
	}
}
