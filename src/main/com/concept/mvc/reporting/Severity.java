package com.concept.mvc.reporting;

public enum Severity {
	INFO, WARNING, CRITICAL;
}
