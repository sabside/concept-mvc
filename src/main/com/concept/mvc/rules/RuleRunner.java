/**
 * 
 */
package com.concept.mvc.rules;

import java.util.Map;

import org.jeasy.rules.api.Facts;
import org.jeasy.rules.api.Rule;
import org.jeasy.rules.api.Rules;
import org.jeasy.rules.api.RulesEngine;
import org.jeasy.rules.core.DefaultRulesEngine;

import com.concept.mvc.exception.ServiceException;

/**
 * @author f3557790
 */
public class RuleRunner {

	public void runRule(Rule rule) throws ServiceException {
		RuleState state = new RuleState();
		Facts facts = new Facts();
		facts.put("state", state);
		
        Rules rules = new Rules();
        rules.register(rule);
        RulesEngine rulesEngine = new DefaultRulesEngine();
        rulesEngine.fire(rules, facts);
       
        state.validate();
	}
	
	public void runRule(Rule rule, Map<String, Object> factMap) throws ServiceException {
		RuleState state = new RuleState();
		Facts facts = new Facts();
		facts.put("state", state);
		
		factMap.forEach((k, val) ->{
			facts.put(k, val);
		});
		
        Rules rules = new Rules();
        rules.register(rule);
        RulesEngine rulesEngine = new DefaultRulesEngine();
        rulesEngine.fire(rules, facts);
       
        state.validate();
	}
}
