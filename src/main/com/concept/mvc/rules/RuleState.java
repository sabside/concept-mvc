/**
 * 
 */
package com.concept.mvc.rules;

import com.concept.mvc.exception.ServiceException;

/**
 * @author f3557790 <sabelo.simelane@fnb.co.za>
 */
public class RuleState {

	private String message = "";

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isPassed(){
		return this.message.isEmpty();
	}
	
	public void validate() throws ServiceException{
		if (!this.isPassed()) throw new ServiceException(this.getMessage());
	}
}
