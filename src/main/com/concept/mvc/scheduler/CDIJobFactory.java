package com.concept.mvc.scheduler;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.inject.spi.CDI;

import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.spi.JobFactory;
import org.quartz.spi.TriggerFiredBundle;


@ApplicationScoped
public class CDIJobFactory implements JobFactory {

	@Override
	public Job newJob(TriggerFiredBundle bundle, Scheduler scheduler) throws SchedulerException {
		final JobDetail jobDetail = bundle.getJobDetail();
		final Class<? extends Job> jobClass = jobDetail.getJobClass();
		try {

			return getBean(jobClass);
		} catch (Exception e) {
			throw new SchedulerException("Problem instantiating class '" + jobDetail.getJobClass().getName() + "'", e);
		}
	}

	private Job getBean(Class jobClazz) {
		final BeanManager bm = CDI.current().getBeanManager();
		final Bean<?> bean = bm.getBeans(jobClazz).iterator().next();
		final CreationalContext<?> ctx = bm.createCreationalContext(bean);
		return (Job) bm.getReference(bean, jobClazz, ctx);
	}
}