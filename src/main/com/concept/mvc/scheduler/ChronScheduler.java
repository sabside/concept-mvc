package com.concept.mvc.scheduler;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

import java.text.ParseException;
import java.util.Date;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Default;
import javax.inject.Inject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.CronExpression;
import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;

import com.concept.mvc.util.ReflectionUtil;
import com.concept.utils.DateTimeUtil;

@Default
@ApplicationScoped
public class ChronScheduler {

	private static final Log log = LogFactory.getLog(ChronScheduler.class);

	@Inject	private CDIJobFactory cdiJobFactory;
	private Scheduler scheduler;

	public void runJobNow(String className) throws Exception {
		ReflectionUtil.invokeMethodInClass(className, "execute", null);
	}

	public static void deleteJob(String className) {
		Scheduler scheduler = null;

		synchronized (className) {
			try {
				scheduler = StdSchedulerFactory.getDefaultScheduler();
			} catch (SchedulerException e) {
				e.printStackTrace();
			}
		}

		try {
			if (scheduler.deleteJob(new JobKey(className))) {
				log.info("Deleted existing job: " + className);
			}
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void scheduleJob(Class jobClass, String cron) {
		try {
			scheduler = StdSchedulerFactory.getDefaultScheduler();
	        scheduler.setJobFactory(cdiJobFactory);
	        final JobDetail job = JobBuilder.newJob(jobClass).withIdentity(jobClass.getName()).build();

	        scheduler.start();
	        Trigger trigger = getGenericTrigger(jobClass, cron); 
	        	        
			scheduler.scheduleJob(job, trigger);
			log.info(String.format("successfuly scheduled job [%s] at [%s]", jobClass, DateTimeUtil.toString(trigger.getFireTimeAfter(new Date()),
					DateTimeUtil.yyyy_MM_dd_HH_mm_ss_SSS)));
			
	      } catch (Exception e) {
	    	  e.printStackTrace();
	      }
	}

	private CronExpression getCronExpression(String cron) {
		CronExpression outputCron = null;

		try {
			outputCron = new CronExpression(cron);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return outputCron;
	}

	private Trigger getGenericTrigger(Class jobClass, String cron) throws ParseException {
		CronExpression cronExpression = getCronExpression(cron);
		if (cronExpression != null) {
			log.info("cron next fire: "+ cronExpression.getNextValidTimeAfter(new Date()));

			return newTrigger().withIdentity(jobClass.getName()).withSchedule(cronSchedule(cronExpression)).forJob(jobClass.getName()).build();
		} else {
			return CronScheduleBuilder.cronScheduleNonvalidatedExpression(cron).build();
		}

	}
}
