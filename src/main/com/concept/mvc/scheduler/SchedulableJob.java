package com.concept.mvc.scheduler;

import org.quartz.Job;

/**
 * 
 * @author Sabside
 *
 */
public interface SchedulableJob extends Job {

	String getCron();
}
