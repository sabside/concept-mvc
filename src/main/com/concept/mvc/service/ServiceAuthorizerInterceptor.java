package com.concept.mvc.service;

import java.io.Serializable;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import com.concept.mvc.exception.ServiceException;

@Service(name = "whatever", roles = { 0 })
@Interceptor
public class ServiceAuthorizerInterceptor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Inject User user;
	public ServiceAuthorizerInterceptor(){}
	
	@AroundInvoke
	public Object authorize(InvocationContext invocationContext) throws Exception {
		
		System.out.println("Entering method: "
                + invocationContext.getMethod().getName() + " in class "
                + invocationContext.getMethod().getDeclaringClass().getName());
		
		Service annotation = invocationContext.getMethod().getDeclaredAnnotation(Service.class);
		String name = annotation.name();
		int[] roles = annotation.roles();
		
		
		boolean allowed = false; 
		for (int i=0; i < roles.length; i ++){
			if (roles[i] == user.getRole()){
				allowed = true;
				break;
			}
		}
		
		if (allowed){
			System.out.println("User is allowed to call service: "+ name);
		} else{
			System.out.println("User is not allowed to call service: "+ name);
			throw new ServiceException("User does not have permission to call this service.");
		}
		
		return invocationContext.proceed();
	}
}
