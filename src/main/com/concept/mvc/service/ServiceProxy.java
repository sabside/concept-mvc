/**
 * 
 */
package com.concept.mvc.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.concept.mvc.exception.ServiceException;
import com.concept.mvc.service.proxy.MethodCallInterceptor;
import com.concept.mvc.service.proxy.Proxies;

/**
 * @author Sabside
 * 
 */
public class ServiceProxy {

	private static MethodCallInterceptor getInterceptor(int roleLevel) {
		List<String> callDetails = new ArrayList<>();
		MethodCallInterceptor interceptor = (proxy, method, args, handler) -> {

			Service serviceAnnotation = proxy.getClass().getInterfaces()[0].getAnnotation(Service.class);

			if (serviceAnnotation != null) {
				ServiceMethod annotation = method.getAnnotation(ServiceMethod.class);
				if (annotation != null) {
					// we check the role
					
					System.out.println("Service Role: " +annotation.role()+" , roleLevel: "+roleLevel);
					if (annotation.role() > roleLevel) {
						throw new ServiceException("You are not allowed to call this service.");
					}
				}
			}

			Object result = handler.invoke(proxy, args);
			callDetails.add(String.format("%s: %s -> %s", method.getName(), Arrays.toString(args), result));
			return result;
		};

		return interceptor;
	}

	public static <T> T createService(T target, Class<T> iface, int roleLevel) {
		return Proxies.interceptingProxy(target, iface, getInterceptor(roleLevel));
	}
}
