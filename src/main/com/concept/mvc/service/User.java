/**
 * 
 */
package com.concept.mvc.service;

import java.io.Serializable;

/**
 * @author Sabelo Simelane <sabside@gmail.com>
 * 
 */
public interface User extends Serializable {

	int getRole();
	
	int getId();
	
	void setRole(int id);
}
