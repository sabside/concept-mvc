/**
 * 
 */
package com.concept.mvc.service.proxy;

/**
 * @author Sabside
 * 
 */
@FunctionalInterface
public interface MethodCallHandler {
	Object invoke(Object proxy, Object[] args) throws Throwable;
}
