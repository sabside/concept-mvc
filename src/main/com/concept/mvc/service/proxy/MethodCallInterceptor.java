/**
 * 
 */
package com.concept.mvc.service.proxy;

import java.lang.reflect.Method;

/**
 * @author Sabside
 * 
 */
@FunctionalInterface
public interface MethodCallInterceptor {

	Object intercept(Object proxy, Method method, Object[] args, MethodCallHandler handler) throws Throwable;

    default MethodCallHandler intercepting(Method method, MethodCallHandler handler) {
        return (proxy, args) -> intercept(proxy, method, args, handler);
    }
    
    public static MethodInterpreter intercepting(MethodInterpreter interpreter, MethodCallInterceptor interceptor) {
        return method -> interceptor.intercepting(method, interpreter.interpret(method));
    }
}
