/**
 * 
 */
package com.concept.mvc.service.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * @author Sabside
 * 
 */
@FunctionalInterface
public interface MethodInterpreter extends InvocationHandler {

	@Override
	default Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		MethodCallHandler handler = interpret(method);
		return handler.invoke(proxy, args);
	}

	MethodCallHandler interpret(Method method);
	
	public static MethodInterpreter caching(MethodInterpreter interpreter) {
	    ConcurrentMap<Method, MethodCallHandler> cache = new ConcurrentHashMap<>();
	    return method -> cache.computeIfAbsent(method, interpreter::interpret);
	}
}
