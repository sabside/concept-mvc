/**
 * 
 */
package com.concept.mvc.service.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Stream;

/**
 * @author Sabside
 * 
 */
public class Proxies {

	public static <T> T interceptingProxy(T target, Class<T> iface, MethodCallInterceptor interceptor) {
		return simpleProxy(iface, caching(intercepting(handlingDefaultMethods(binding(target)), interceptor)));
	}

	public static MethodInterpreter binding(Object target) {
		return binding(target, method -> {
			throw new IllegalStateException(String.format("Target class %s does not support method %s", target.getClass(), method));
		});
	}

	public static MethodInterpreter handlingDefaultMethods(MethodInterpreter nonDefaultInterpreter) {
		return method -> method.isDefault() ? DefaultMethodCallHandler.forMethod(method) : nonDefaultInterpreter.interpret(method);
	}

	public static MethodInterpreter intercepting(MethodInterpreter interpreter, MethodCallInterceptor interceptor) {
		return method -> interceptor.intercepting(method, interpreter.interpret(method));
	}

	public static MethodInterpreter binding(Object target, MethodInterpreter unboundInterpreter) {
		return method -> {
			if (method.getDeclaringClass().isAssignableFrom(target.getClass())) {
				return (proxy, args) -> method.invoke(target, args);
			}

			return unboundInterpreter.interpret(method);
		};
	}

	public static MethodInterpreter caching(MethodInterpreter interpreter) {
		ConcurrentMap<Method, MethodCallHandler> cache = new ConcurrentHashMap<>();
		return method -> cache.computeIfAbsent(method, interpreter::interpret);
	}

	public static <T> T simpleProxy(Class<? extends T> iface, InvocationHandler handler, Class<?>... otherIfaces) {
		Class<?>[] allInterfaces = Stream.concat(Stream.of(iface), Stream.of(otherIfaces)).distinct().toArray(Class<?>[]::new);

		return (T) Proxy.newProxyInstance(iface.getClassLoader(), allInterfaces, handler);
	}
}
