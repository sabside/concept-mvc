package com.concept.mvc.util;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.concept.mvc.viewbean.ViewBean;

public class BeanUtil {

	private static final Log log = LogFactory.getLog(BeanUtil.class);

	public static void setProperty(Object bean, Field field, Object value) throws IllegalAccessException, InvocationTargetException {
		Method[] allMethods = bean.getClass().getDeclaredMethods();
		for (Method m : allMethods) {
			if (m.getName().equals("set" + field.getName())) {
				if (field.getType().toString().contains("Integer") || field.getType().toString().contains("int")) {
					m.invoke(bean, Integer.parseInt((String) value));
				} else if (field.getType().toString().contains("Long") || field.getType().toString().contains("long")) {
					m.invoke(bean, Long.parseLong((String) value));
				} else {
					m.invoke(bean, value);
				}
			}
		}
	}

	public static Field getFieldFromBean(ViewBean viewbean, String fieldName) {
		Field[] fields = viewbean.getClass().getDeclaredFields();
		for (int i = 0; i < fields.length; i++) {
			if (fields[i].getName().equals(fieldName)) {
				log.info("field name: " + fieldName);
				return fields[i];
			}
		}
		return null;
	}
	
	/**
	 * Does a deep copy. i.e. also clones children
	 * @param fromBean
	 * @return
	 */
	public static Object clone(Object fromBean) {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		XMLEncoder out = new XMLEncoder(bos);
		out.writeObject(fromBean);
		out.close();
		ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
		XMLDecoder in = new XMLDecoder(bis);
		Object toBean = in.readObject();
		in.close();
		return toBean;
	}
}
