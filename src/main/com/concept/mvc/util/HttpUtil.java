package com.concept.mvc.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.Map;

import javax.xml.ws.http.HTTPException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

@SuppressWarnings("rawtypes")
public class HttpUtil {
	private static final Log log = LogFactory.getLog(HttpUtil.class);

	private static final int DEFAULT_TIMEOUT = 20000;
	private static final String EQ = "=";
	private static final String AND = "&";

	public static String convertMapToURLParms(Map<String, String> parms) throws UnsupportedEncodingException {
		StringBuffer result = new StringBuffer();

		Iterator iterator = parms.entrySet().iterator();
		while (iterator.hasNext()) {
			Map.Entry mapEntry = (Map.Entry) iterator.next();
			result.append(mapEntry.getKey()).append(EQ).append(URLEncoder.encode(mapEntry.getValue().toString(), "UTF-8")).append(AND);
		}

		String resultStr = result.toString();
		if (resultStr.endsWith(AND)){
			resultStr = resultStr.substring(0, resultStr.length()-1);
		}
		
		return resultStr;
	}

	public static String GET(String url, Map<String, String> headers) throws MalformedURLException, IOException {
		return (HttpUtil.GET(url, DEFAULT_TIMEOUT, headers));
	}

	public static String GET(String url) throws MalformedURLException, IOException {
		return (HttpUtil.GET(url, DEFAULT_TIMEOUT, null));
	}

	public static String GET(String url, int timeout, Map<String, String> headers) throws MalformedURLException, IOException {
		try {
			log.debug("GET(url[" + url + "])");
			URL u = new URL(url);
			URLConnection connection = u.openConnection();
			connection.setConnectTimeout(timeout);
			connection.setReadTimeout(timeout);

			// We add headers if any
			if (headers != null && headers.size() > 0) {
				Iterator iterator = headers.entrySet().iterator();
				while (iterator.hasNext()) {
					Map.Entry mapEntry = (Map.Entry) iterator.next();
					connection.setRequestProperty(mapEntry.getKey().toString(), mapEntry.getValue().toString());
				}
			}

			String line;
			StringBuilder builder = new StringBuilder();
			BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			while ((line = reader.readLine()) != null) {
				builder.append(line);
			}

			String response = builder.toString();
			log.debug("Response[" + response + "]");
			return (response);
		} finally {
		}

	}

	public static String GET(String url, int timeout) throws MalformedURLException, IOException {
		return GET(url, timeout, null);
	}

	public static String POST(String url) throws MalformedURLException, IOException {
		return (HttpUtil.POST(url, null, DEFAULT_TIMEOUT, null));
	}

	public static String POST(String url, String json) throws MalformedURLException, IOException {
		return (HttpUtil.POST(url, json, DEFAULT_TIMEOUT, null));
	}

	public static String POST(String url, String json, int timeout, Map<String, String> headers) throws MalformedURLException, IOException {
		try {
			log.debug("POST(url[" + url + "], json[" + json + "])");
			URL u = new URL(url);
			HttpURLConnection connection = (HttpURLConnection) u.openConnection();
			connection.setConnectTimeout(timeout);
			connection.setReadTimeout(timeout);

			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
			connection.setDoOutput(true);
			connection.setInstanceFollowRedirects(false);

			// We add headers if any
			if (headers != null && headers.size() > 0) {
				Iterator iterator = headers.entrySet().iterator();
				while (iterator.hasNext()) {
					Map.Entry mapEntry = (Map.Entry) iterator.next();
					connection.setRequestProperty(mapEntry.getKey().toString(), mapEntry.getValue().toString());
				}
			}

			if (json != null) {
				OutputStream os = connection.getOutputStream();
				os.write(json.getBytes());
				os.flush();
			}

			String line;
			StringBuilder builder = new StringBuilder();
			BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			while ((line = reader.readLine()) != null) {
				builder.append(line);
			}

			String response = builder.toString();
			log.debug("Response[" + response + "]");
			return (response);
		} finally {
		}
	}

	public static String POST(String url, String json, Map<String, String> headers) throws MalformedURLException, IOException {
		return (HttpUtil.POST(url, json, DEFAULT_TIMEOUT, headers));
	}

	public static String POST(String url, String json, int timeout) throws MalformedURLException, IOException {
		return HttpUtil.POST(url, json, timeout, null);
	}

	public static void DELETE(String url, Map<String, String> headers) throws MalformedURLException, IOException {
		HttpUtil.DELETE(url, DEFAULT_TIMEOUT, headers);
	}

	public static void DELETE(String url, int timeout, Map<String, String> headers) throws MalformedURLException, IOException {
		try {
			log.debug("DELETE(url[" + url + "])");
			URL u = new URL(url);
			HttpURLConnection connection = (HttpURLConnection) u.openConnection();
			connection.setConnectTimeout(timeout);
			connection.setReadTimeout(timeout);

			connection.setRequestMethod("DELETE");
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setDoOutput(true);
			connection.setInstanceFollowRedirects(false);

			// We add headers if any
			if (headers != null && headers.size() > 0) {
				Iterator iterator = headers.entrySet().iterator();
				while (iterator.hasNext()) {
					Map.Entry mapEntry = (Map.Entry) iterator.next();
					connection.setRequestProperty(mapEntry.getKey().toString(), mapEntry.getValue().toString());
				}
			}

			connection.connect();

			if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
				throw new HTTPException(connection.getResponseCode());
			}
		} finally {
		}
	}

	public static void DELETE(String url) throws MalformedURLException, IOException {
		HttpUtil.DELETE(url, DEFAULT_TIMEOUT, null);
	}

	public static void DELETE(String url, int timeout) throws MalformedURLException, IOException {
		HttpUtil.DELETE(url, DEFAULT_TIMEOUT, null);
	}
}
