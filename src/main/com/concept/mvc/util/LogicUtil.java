package com.concept.mvc.util;

public class LogicUtil {

	public static Object checkIfNull(Object object, Object defaultObject) {
		return object == null ? defaultObject : object;
	}
}
