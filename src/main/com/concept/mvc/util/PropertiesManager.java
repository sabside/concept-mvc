package com.concept.mvc.util;

import java.io.FileInputStream;
import java.util.Properties;

public class PropertiesManager {

	private static PropertiesManager instance;
	private String dataPath;

	private Properties systemProperties;

	public static PropertiesManager getInstance() {
		if (instance == null) {
			instance = new PropertiesManager();
		}
		return instance;
	}

	public void setDataPath(String dataPath) {
		this.dataPath = dataPath;
	}

	public String getProperty(String key){
		if (systemProperties == null){
			systemProperties = getSystemProperties();	
		}
			
		return systemProperties.getProperty(key);
	}
	
	public String getProperty(String key, String defaultVal){
		if (systemProperties == null){
			systemProperties = getSystemProperties();	
		}
			
		return systemProperties.getProperty(key, defaultVal);
	}
	
	public Properties getSystemProperties() {
		if (systemProperties != null) {
			return systemProperties;
		}

		Properties props = new Properties();
		String propFile = dataPath + "/system.properties";
		try {
			props.load(new FileInputStream(propFile));
			systemProperties = props;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return props;
	}

	public void setSystemProperties(Properties systemProperties) {
		this.systemProperties = systemProperties;
	}

}
