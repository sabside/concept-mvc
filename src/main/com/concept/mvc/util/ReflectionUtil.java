package com.concept.mvc.util;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

public class ReflectionUtil {

	public static Object invokeMethodInClass(String className, String methodToInvoke, Object[] args) throws Exception {
		Class<?> c = Class.forName(className);
		Object t = c.newInstance();
		Object response = null;

		Class[] argTypes = null;
		if (args != null) {
			argTypes = new Class[args.length];
			for (int i = 0; i < argTypes.length; i++) {
				argTypes[i] = argTypes[i].getClass();
			}

			Method method = c.getDeclaredMethod(methodToInvoke, argTypes);
			response = method.invoke(t, args);
		} else {
			args = new Object[] { null };

			for (Method method : c.getDeclaredMethods()) {
				if (method.getName().equals(methodToInvoke)) {
					response = method.invoke(t, args);
					break;
				}
			}
		}

		return response;
	}
	
	public static List<Class<?>> findClassesImplementing(final Class<?> interfaceClass, final String fromPackage) {

		if (interfaceClass == null) {
			System.out.println("Unknown subclass.");
			return null;
		}

		if (fromPackage == null) {
			System.out.println("Unknown package.");
			return null;
		}

		final List<Class<?>> rVal = new ArrayList<Class<?>>();
		try {
			final Class<?>[] targets = getAllClassesFromPackage(fromPackage);
			if (targets != null) {
				for (Class<?> aTarget : targets) {
					if (aTarget == null) {
						continue;
					} else if (aTarget.equals(interfaceClass)) {
						System.out.println("Found the interface definition.");
						continue;
					} else if (!interfaceClass.isAssignableFrom(aTarget)) {
						// System.out.println("Class '" + aTarget.getName() + "' is not a " + interfaceClass.getName());
						continue;
					} else {
						rVal.add(aTarget);
					}
				}
			}
		} catch (ClassNotFoundException e) {
			System.out.println("Error reading package name.");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Error reading classes in package.");
			e.printStackTrace();
		}

		return rVal;
	}

	/**
	 * Load all classes from a package.
	 * 
	 * @param packageName
	 * @return
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public static Class[] getAllClassesFromPackage(final String packageName) throws ClassNotFoundException, IOException {
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		assert classLoader != null;
		String path = packageName.replace('.', '/');
		Enumeration<URL> resources = classLoader.getResources(path);
		List<File> dirs = new ArrayList<File>();
		while (resources.hasMoreElements()) {
			URL resource = resources.nextElement();
			dirs.add(new File(resource.getFile()));
		}
		ArrayList<Class> classes = new ArrayList<Class>();
		for (File directory : dirs) {
			classes.addAll(findClasses(directory, packageName));
		}
		return classes.toArray(new Class[classes.size()]);
	}

	/**
	 * Find file in package.
	 * 
	 * @param directory
	 * @param packageName
	 * @return
	 * @throws ClassNotFoundException
	 */
	public static List<Class<?>> findClasses(File directory, String packageName) throws ClassNotFoundException {
		List<Class<?>> classes = new ArrayList<Class<?>>();
		if (!directory.exists()) {
			return classes;
		}
		File[] files = directory.listFiles();
		if (files != null) {
			for (File file : files) {
				if (file.isDirectory()) {
					assert !file.getName().contains(".");
					classes.addAll(findClasses(file, packageName + "." + file.getName()));
				} else if (file.getName().endsWith(".class")) {
					String className = packageName + '.' + file.getName().substring(0, file.getName().length() - 6);
					if (className.startsWith(".")) {
						className = className.replaceFirst(".", "");

					}
					classes.add(Class.forName(className));
				}
			}
		}
		return classes;
	}

	public static Annotation getAnnotationFromClass(Object object, Class targetAnnotation) {
		Class aClass = object.getClass();

		return aClass.getAnnotation(targetAnnotation);
	}
	
}
