package com.concept.mvc.util;

public class Request {

	public static final String USERS = "users";
	public static final String DESIGNATIONS = "designations";
	public static final String USER = "user";
	public static final String JSON = "JSON";
	public static final String LAWFIRM = "lawfirm";
	public static final String CLIENTS = "clients";
	public static final String CLIENT = "client";
}
