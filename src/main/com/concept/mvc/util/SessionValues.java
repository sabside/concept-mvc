package com.concept.mvc.util;

public class SessionValues {
	public static final String USER = "user";
	public static final String ERROR_MESSAGES = "ErrorMessages";
	public static final String OTP = "OTP";
	public static final String NAVIGATOR = "NAVIGATOR";  
	public static final String AUTHENTICATED = "AUTHENTICATED";
}
