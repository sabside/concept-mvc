package com.concept.mvc.util;

public class StringUtil {

	public static String EMPTY_STRING = "";
	
	/**
	 * Verifies a string value by checking for null and empty.
	 * 
	 * @param value
	 * @throws IllegalArgumentException
	 */
	public static void verifyString(String value) throws IllegalArgumentException {
		if (value == null || value.isEmpty()) {
			throw new IllegalArgumentException("value passed is null!!");
		}
	}

	public static boolean isValid(String value) {
		if (value == null || value.isEmpty()) {
			return false;
		}
		return true;
	}
	
	public static String lowerFirst(String string) {
		return string.substring(0, 1).toLowerCase() + string.substring(1);
	}
}
