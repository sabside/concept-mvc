package com.concept.mvc.validation;

public class ErrorMessage {

	private String fieldId;
	private String fieldDisplayName;
	private String message = "";

	public static ErrorMessage newInstance(){
		return new ErrorMessage();
	}
	
	public String getFieldId() {
		return fieldId;
	}

	public void setFieldId(String fieldId) {
		this.fieldId = fieldId;
	}

	public String getFieldDisplayName() {
		return fieldDisplayName;
	}

	public void setFieldDisplayName(String fieldDisplayName) {
		this.fieldDisplayName = fieldDisplayName;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
