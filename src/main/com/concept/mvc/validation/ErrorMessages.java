package com.concept.mvc.validation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class ErrorMessages {
	private Map<String, ErrorMessage> messages = new HashMap<String, ErrorMessage>();

	public Map<String, ErrorMessage> getMessages() {
		return messages;
	}

	public void addMessage(String fieldId, ErrorMessage message) {
		messages.put(fieldId, message);
	}
	
	public static List<ErrorMessage> toList(Map<String, ErrorMessage> messages){
		List<ErrorMessage> list = new ArrayList<ErrorMessage>();
		Iterator iterator = messages.entrySet().iterator();
		while (iterator.hasNext()) {
			Map.Entry mapEntry = (Map.Entry) iterator.next();
			ErrorMessage error = (ErrorMessage) mapEntry.getValue();
			list.add(error);
		}
		return list;
	}
}
