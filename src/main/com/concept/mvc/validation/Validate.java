package com.concept.mvc.validation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Validate {

	public boolean mandatory() default true;
	public int minLength() default 0;
	public int maxLength() default Integer.MAX_VALUE;
	public String displayName() default "";
	public String fieldId() default "";
	public String dependsOn() default "";
	
}
