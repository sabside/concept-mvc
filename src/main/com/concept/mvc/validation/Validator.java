package com.concept.mvc.validation;

import java.lang.reflect.Field;

import com.concept.mvc.messages.MessageBundle;
import com.concept.mvc.util.StringUtil;
import com.concept.utils.exception.ValidationException;

public class Validator {

	public static void validateNullOrEmpty(String value, String fieldName) throws Exception {
		try {
			StringUtil.verifyString(value);
		} catch (Exception e) {
			throw new Exception(MessageBundle.getInstance().getMessage("002") + " " + fieldName);
		}
	}

	public static ErrorMessage validateField(Field field, String value) throws ValidationException {
		ErrorMessage message = null;

		if (field.isAnnotationPresent(Validate.class)) {
			message = ErrorMessage.newInstance();

			Validate annotation = (Validate) field.getAnnotation(Validate.class);

			// MANDATORY
			if (true) { //WE Always assume it's mandatory here
				if (!StringUtil.isValid(value)) {
					message.setFieldId(annotation.fieldId());
					message.setFieldDisplayName(annotation.displayName());
					message.setMessage(MessageBundle.getInstance().getMessage("002") + " " + message.getFieldDisplayName());
				}
			}

			// MIN LENGTH
			if (value != null && value.length() < annotation.minLength() && message.getMessage().isEmpty()) {

				// We ignore this if the value is empty. We assume it's not mandatory
				if (!value.isEmpty()) {
					message.setFieldId(annotation.fieldId());
					message.setFieldDisplayName(annotation.displayName());
					message.setMessage("The field '" + message.getFieldDisplayName() + "' is too short.");
				}
			}

			// MAX LENGTH
			if (value != null && value.length() > annotation.maxLength() && message.getMessage().isEmpty()) {
				message.setFieldId(annotation.fieldId());
				message.setFieldDisplayName(annotation.displayName());
				message.setMessage("The field '" + message.getFieldDisplayName() + "' is too long.");
			}
		}

		return message;
	}
}
