/**
 * 
 */
package com.concept.mvc.version;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.concept.mvc.navigation.controller.Controller;

/**
 * @author Sabside
 *
 */
public final class Version {
	private static final Log log = LogFactory.getLog(Version.class);

	private static final String versionFile = "build.version";

	public static final String getVersionNumber() {
		String version = "";
		try {
			Properties props = new Properties();
			String propFile = Controller.serverPath.replace("/WEB-INF", "") + "/" + versionFile;

			try {
				props.load(new FileInputStream(propFile));

			} catch (FileNotFoundException e) {
				log.warn("File not found :" + e.getMessage());
			} catch (Exception e) {
				e.printStackTrace();
			}
			version = props.getProperty("version.number") + "." + props.getProperty("build.number");
			log.info("Build "+version);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return version;
	}
}
