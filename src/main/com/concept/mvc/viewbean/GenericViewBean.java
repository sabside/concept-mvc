/**
 * 
 */
package com.concept.mvc.viewbean;

/**
 * @author Sabside
 *
 */
public class GenericViewBean implements ViewBean {

	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
