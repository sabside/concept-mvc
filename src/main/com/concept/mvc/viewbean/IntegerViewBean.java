package com.concept.mvc.viewbean;


public class IntegerViewBean implements ViewBean {

	private int intValue;

	public int getIntValue() {
		return intValue;
	}

	public void setIntValue(int intValue) {
		this.intValue = intValue;
	}

}
