package com.concept.mvc.viewbean;


public class JsonViewBean implements ViewBean {
	private String nav;

	public String getNav() {
		return nav;
	}

	public void setNav(String nav) {
		this.nav = nav;
	}

}
