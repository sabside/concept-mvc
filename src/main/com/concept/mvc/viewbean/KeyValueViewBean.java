/**
 * 
 */
package com.concept.mvc.viewbean;

/**
 * @author Sabelo Simelane <sabside@gmail.com>
 * 
 */
public class KeyValueViewBean implements ViewBean {

	private String id;
	private String value;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
