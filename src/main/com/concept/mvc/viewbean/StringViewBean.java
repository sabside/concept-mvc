/**
 * 
 */
package com.concept.mvc.viewbean;

/**
 * @author Sabside
 * 
 */
public class StringViewBean implements ViewBean {

	private String stringValue;

	public String getStringValue() {
		return stringValue;
	}

	public void setStringValue(String stringValue) {
		this.stringValue = stringValue;
	}

}
