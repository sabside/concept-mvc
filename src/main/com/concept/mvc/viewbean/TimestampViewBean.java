/**
 * 
 */
package com.concept.mvc.viewbean;

import java.util.Date;

import com.concept.utils.DateTimeUtil;

/**
 * @author Sabelo Simelane <sabside@gmail.com>
 * 
 */
public class TimestampViewBean implements ViewBean {

	private final Date inputDate;
	private String date;
	private String timestamp;
	private String pretty;
	private String prettyshort;

	public static TimestampViewBean convert(Date date) {
		TimestampViewBean bean = new TimestampViewBean(date);
		if (date == null) return bean;
		try {
			bean.setDate(DateTimeUtil.toString(date));
			bean.setTimestamp(DateTimeUtil.toString(date, DateTimeUtil.yyyy_MM_dd_HH_mm));
			bean.setPretty(DateTimeUtil.toString(date, DateTimeUtil.PRETTY));
			bean.setPrettyshort(DateTimeUtil.toString(date, DateTimeUtil.PRETTY_SHORT));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bean;
	}

	public TimestampViewBean(Date timestamp) {
		this.inputDate = timestamp;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getPretty() {
		return pretty;
	}

	public void setPretty(String pretty) {
		this.pretty = pretty;
	}

	public String getPrettyshort() {
		return prettyshort;
	}

	public void setPrettyshort(String prettyshort) {
		this.prettyshort = prettyshort;
	}

	public Date getInputDate() {
		return inputDate;
	}

}
