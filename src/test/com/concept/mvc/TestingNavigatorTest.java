/**
 * 
 */
package com.concept.mvc;

import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.concept.mvc.navigation.TestingNavigator;
import com.concept.mvc.viewbean.StringViewBean;
import com.concept.utils.CDIUtil;

/**
 * @author f3557790
 * 
 */
@RunWith(WeldJUnit4Runner.class)
public class TestingNavigatorTest {

	TestingNavigator nav;
	@Inject CDIUtil cdiUtil;
	
	@Before
	public void setup() throws 	Exception {
		
		nav = (TestingNavigator) cdiUtil.newInstance(TestingNavigator.class, TestingNavigator.class.getName());
	}
	
	@Test
	public void testNavigator() throws Exception {
		
		StringViewBean bean = new StringViewBean();
		bean.setStringValue("hello");
		nav.execute(bean);
	}
}
