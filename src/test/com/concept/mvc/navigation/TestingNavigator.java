/**
 * 
 */
package com.concept.mvc.navigation;

import com.concept.mvc.TestingNavigatorTest;
import com.concept.mvc.cache.Cache;
import com.concept.mvc.navigation.controller.Navigate;
import com.concept.mvc.navigation.controller.Navigator;
import com.concept.mvc.navigation.controller.ReturnType;
import com.concept.mvc.viewbean.StringViewBean;

/**
 * @author f3557790
 * @param <ViewBean>
 * 
 */
@Cache(keys="id", refreshon={TestingNavigator.class, TestingNavigatorTest.class})
@Navigate(returnType=ReturnType.JSON)
public class TestingNavigator implements Navigator<StringViewBean> {
	
	@Override
	public void execute(StringViewBean viewbean) throws NavigatorException {
		try {

	
		} catch (Exception e) {
		}
	}
}
